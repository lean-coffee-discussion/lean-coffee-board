# Lean Coffee Board

## Running Tests

```
npm run test
```

## Building the Application

```
npm run build
```

The following environment variables must be set to build the application correctly for any environment other than a local deployment:

```
REACT_APP_BASENAME
REACT_APP_BASE_API_URL
PUBLIC_URL
```

`REACT_APP_BASENAME` is the base path of the deployed application. If the application is deployed to `http://example.com/lean-coffee-board` then the base name should be set to `lean-coffee-board`.

`REACT_APP_BASE_API_URL` is the URL for the application API (https://gitlab.com/lean-coffee-discussion/lean-coffee-server)[(Lean Coffee Server)].

`PUBLIC_URL` is the URL for the deployed application. In the above example this would be set to `http://example.com/lean-coffee-board`.