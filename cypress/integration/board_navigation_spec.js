

describe('Lean coffee board', function () {
  it('Create board, create topic, move topic', function () {
    cy.visit('/');
    cy.get('.bm-burger-button > button').click();
    cy.get('.Add-Board-input').type('Test Board');
    cy.get('.Add-Board-button').click();
    var linkPath = '';
    cy.get('.List-Board-link').then((link) => {
      linkPath = link[0].pathname;
    }).click().then(() => {
      return cy.url().should('include', linkPath);
    }).then(() => {
      return cy.get('.bm-menu').should('not.be.visible');
    }).then(() => {
      return cy.get('.Add-Topic-input').type('new test topic');
    }).then(() => {
      return cy.get('.Add-Topic-button').click();
    }).then(() => {
      return cy.get('.dot-vote').click();
    }).then(() => {
      return cy.get('.move-right > img').click();
    }).then(() => {
      return cy.get('.move-right > img').click();
    }).then(() => {
      return cy.get('.move-left > img').click();
    }).then(() => {
      return cy.get('.Add-Topic-input').type('second test topic');
    }).then(() => {
      return cy.get('.Add-Topic-button').click();
    }).then(() => {
      return cy.get(':nth-child(1) > :nth-child(1) > :nth-child(1) > .List-Topics > .List-Topics-list > li > :nth-child(1) > .move-right > img').click()
    }).then(() => {
      return cy.get(':nth-child(2) > :nth-child(1) > .move-right > img').click();
    }).then(() => {
      return cy.get(':nth-child(3) > :nth-child(1) > :nth-child(1) > .List-Topics > .List-Topics-list > li > :nth-child(1) > div').then((topicElement) => {
        expect(topicElement[0].innerText).to.include('second test topic');
      });
    });
  });
});