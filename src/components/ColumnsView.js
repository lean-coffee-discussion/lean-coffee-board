import React, { Component } from 'react';
import _ from 'lodash';
import './ColumnsView.css';

class ColumnsView extends Component {
  render() {
    const percentWidth = 100 / this.props.columnsIdArray.length;
    var templateString = ""
    _.map(this.props.columnsIdArray, () => { templateString += percentWidth.toFixed(0) + "% " });
    return (
      <div className="columns-view" style={{ gridTemplateColumns: templateString }} >
        {_.map(this.props.columnsIdArray, this.renderColumnById.bind(this))}
      </ div>
    )
  }

  renderColumnById(columnId) {
    return (
      <div className="columns-view-column" key={columnId}>
        {this.props.renderColumn(_.get(this.props.columnsById, columnId))}
      </div>
    )
  }
}

export default ColumnsView;