import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TextInput extends Component {
  render() {
    return (
      <input
        type='text'
        placeholder={this.props.placeholder}
        className={this.props.className}
        value={this.props.value}
        onChange={this.props.onChange}
        ref={this.props.reference}
      />
    )
  }
}

TextInput.propTypes = {
  className: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string
}


export default TextInput;