import React from 'react';
import { shallow } from 'enzyme';
import TextInput from './TextInput';

const testInput = 'Hello React';
const testClass = 'text-input-class';
const testPlaceholder = 'Placeholder Text'
function setup(value = '') {
  const props = {
    className: testClass,
    value: value,
    onChange: jest.fn(),
    placeholder: testPlaceholder
  }

  const enzymeWrapper = shallow(<TextInput {...props} />);

  return {
    props,
    enzymeWrapper
  }
}

describe('Components', () => {
  describe('TextInput', () => {
    it('should render an empty text field', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('input').text()).toBe('');
    });

    it('should add the class that is passed in through the properties', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('input').hasClass(testClass)).toBeTruthy();
    });

    it('should render a text field with the passed in value', () => {
      const { enzymeWrapper } = setup(testInput);
      expect(enzymeWrapper.find('input').props().value).toBe(testInput);
    });

    it('should render a text field with an onChange function', () => {
      const { enzymeWrapper, props } = setup();
      expect(props.onChange.mock.calls.length).toBe(0);
      enzymeWrapper.find('input').simulate('change');
      expect(props.onChange.mock.calls.length).toBe(1);
    });

    it('should show a placeholder when defined', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.getElement().props.placeholder).toEqual(testPlaceholder);
    })
  });
});