import { slide as Menu } from 'react-burger-menu';
import { decorator as ReduxBurgerMenu } from 'redux-burger-menu';

export default ReduxBurgerMenu(Menu); 