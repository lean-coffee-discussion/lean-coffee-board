import React from 'react';
import { shallow } from 'enzyme';
import ButtonWithText from './ButtonWithText';

const buttonText = 'Hello React';
const buttonClass = 'test-button-class';
function setup() {
  const props = {
    className: buttonClass,
    clickHandler: jest.fn(),
    text: buttonText
  }

  const enzymeWrapper = shallow(<ButtonWithText {...props} />);

  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('ButtonWithText', () => {
    it('should render self and sub-components', () => {
      const { enzymeWrapper } = setup();
      const button = enzymeWrapper.find('button');
      expect(button.exists()).toBe(true);

      expect(button.text()).toEqual(buttonText);
    });

    it('should add the class that is passed in through the properties', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('button').hasClass(buttonClass)).toBeTruthy();
    });

    it('should call the button clicked handler', () => {
      const { enzymeWrapper, props } = setup();
      expect(props.clickHandler.mock.calls.length).toBe(0);
      enzymeWrapper.find('button').simulate('click');
      expect(props.clickHandler.mock.calls.length).toBe(1);
    });
  });
});