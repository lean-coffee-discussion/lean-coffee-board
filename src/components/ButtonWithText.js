import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ButtonWithText extends Component {
  buttonClicked() {
    this.props.clickHandler();
  }

  render() {
    return (
      <button className={this.props.className} onClick={this.buttonClicked.bind(this)}>{this.props.text}</button>
    );
  }
}

ButtonWithText.propTypes = {
  className: PropTypes.string,
  clickHandler: PropTypes.func,
  text: PropTypes.string
}

export default ButtonWithText;