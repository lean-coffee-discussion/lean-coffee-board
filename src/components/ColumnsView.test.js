import React from 'react';
import { shallow } from 'enzyme';
import ColumnsView from './ColumnsView';

const columnsById = {
  'abc-123': {
    id: 'abc-123',
    title: 'test 1'
  },
  'def-123124': {
    id: 'def-123124',
    title: 'test 2'
  },
  'qwe-2345': {
    id: 'qwe-2345',
    title: 'test 3'
  }
};
const columnsIdArray = ['abc-123', 'def-123124', 'qwe-2345']
const columnCount = columnsIdArray.length;
const testRenderColumn = (row) => {
  return (<div className='testClass' />)
}

var wrapper;

describe('Columns View Component', () => {
  beforeEach(() => {
    wrapper = shallow(<ColumnsView columnsIdArray={columnsIdArray} columnsById={columnsById} renderColumn={testRenderColumn} />);
  });

  it('should render multiple column components', () => {
    expect(wrapper.find('.testClass').length).toEqual(columnCount);
  });
});