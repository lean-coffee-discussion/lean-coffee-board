import React from 'react';
import { shallow } from 'enzyme';
import ListView from './ListView';
describe('Components', () => {
  describe('ListView', () => {

    it('renders a list of items', () => {
      const testRowsIdArray = ['abc-123', 'def-456', 'ghi-789'];
      const testRowsById = {
        'abc-123': {
          id: 'abc-123',
          title: 'Item 1'
        },
        'def-456': {
          id: 'def-456',
          title: 'Item 2'
        },
        'ghi-789': {
          id: 'ghi-789',
          title: 'Item 3'
        }
      };
      const testRenderRow = (row) => {
        return (<div />)
      }
      const component = shallow(
        <ListView rowsIdArray={testRowsIdArray}
          rowsById={testRowsById}
          renderRow={testRenderRow} />
      );

      const listElements = component.find('li');
      expect(listElements.length).toEqual(3);
    });

    it('should add the class that is passed in through the properties', () => {
      const testClass = "Test-Class-Name";
      const testRowsIdArray = [];
      const testRowsById = {};
      const testRenderRow = (row) => {
        return (<div />)
      }
      const component = shallow(
        <ListView
          className={testClass}
          rowsIdArray={testRowsIdArray}
          rowsById={testRowsById}
          renderRow={testRenderRow} />
      );
      expect(component.find('ul').hasClass(testClass)).toBeTruthy();
    });
  });
});

