import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import configureMockStore from 'redux-mock-store';
import { ConnectedRouter } from 'react-router-redux';
import * as testState from './store/testState.test';
import createHistory from 'history/createBrowserHistory';

const store = configureMockStore()(testState.state);
const history = createHistory();

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><ConnectedRouter history={history}><App /></ConnectedRouter></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
