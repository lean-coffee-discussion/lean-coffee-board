import React from 'react';
import { shallow, mount } from 'enzyme';
import Board from './Board';
import configureMockStore from 'redux-mock-store';
import { apiMiddleware } from 'redux-api-middleware';
import thunk from 'redux-thunk';
import * as types from '../store/actionTypes';
import * as testState from '../store/testState.test';
import _ from 'lodash';

const firstDiscussionTitle = _.get(testState.state.discussions.byId, testState.firstDiscussionId).title
const middleware = [apiMiddleware, thunk];
const mockStore = configureMockStore(middleware);
let store;
let wrapper;

describe('Board', () => {
  let match;
  beforeEach(() => {
    match = {
      params: { boardId: testState.firstBoardId }
    };
    store = mockStore(testState.state);
    wrapper = shallow(<Board match={match} />, { context: { store } }).dive();
  });

  it('should render a board', () => {
    const emptyWrapper = shallow(<Board />, { context: { store } });
    expect(emptyWrapper.exists).toBeTruthy();
  });

  it('should contain three discussions for topics', () => {
    const discussionWrapper = wrapper.find('ColumnsView').dive().find('Connect(Discussion)');
    expect(discussionWrapper.length).toEqual(3);
    expect(discussionWrapper.first().dive().props().title).toEqual(firstDiscussionTitle);
    expect(discussionWrapper.first().dive().props().discussionId).toEqual(testState.firstDiscussionId);
  });

  it('should display the discussion timer', () => {
    expect(wrapper.find('Connect(CountdownTimer)').exists()).toBeTruthy();
  });

  it('should contain a component to add a new topic to the first board', () => {
    const addTopicWrapper = wrapper.find('Connect(AddTopic)');
    expect(addTopicWrapper.exists()).toBeTruthy();
    expect(addTopicWrapper.dive().props().discussionId).toBe(testState.firstDiscussionId);
  });

  it('should call the move right function when the move right button is clicked', () => {
    const expectedAction = {
      type: types.MOVE_TOPIC,
      payload: {
        topicId: testState.firstTopicId,
        sourceDiscussionId: testState.firstDiscussionId,
        destDiscussionId: testState.secondDiscussionId
      }
    };
    const boardWrapper = mount(<Board match={match} />, { context: { store } });
    boardWrapper.find('.move-right').first().simulate('click');
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should not dispatch move right action if on the last discussion', () => {
    const boardWrapper = mount(<Board match={match} />, { context: { store } });
    const originalActions = store.getActions().length;
    boardWrapper.find('.move-right').last().simulate('click');
    expect(store.getActions().length).toBe(originalActions);
  });

  it('should call the move left function when the move left button is clicked', () => {
    const expectedAction = {
      type: types.MOVE_TOPIC,
      payload: {
        topicId: testState.lastTopicId,
        sourceDiscussionId: testState.lastDiscussionId,
        destDiscussionId: testState.secondDiscussionId
      }
    };
    const boardWrapper = mount(<Board match={match} />, { context: { store } });
    boardWrapper.find('.move-left').last().simulate('click');
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should not dispatch move left action if on the first discussion', () => {
    const boardWrapper = mount(<Board match={match} />, { context: { store } });
    const originalActions = store.getActions().length;
    boardWrapper.find('.move-left').first().simulate('click');
    expect(store.getActions().length).toBe(originalActions);
  });

  it('should fetch board when component loads', () => {
    const expectedActions = [
      { type: types.GET_BOARD_REQUEST }
    ];
    expect(store.getActions().length).toBe(1);
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should handle empty state', () => {
    const emptyStore = mockStore({});
    const boardWrapper = mount(<Board match={match} />, { context: { store: emptyStore } });
    expect(boardWrapper.exists).toBeTruthy();
    expect(boardWrapper.find('Board').text()).toBe('Error Loading Application State!');
  });

  it('should update a discussion on the server if a topic is moved', async () => {
    let loggedInState = testState.state;
    loggedInState.user.token = 'abc';
    loggedInState.user.isLoggedIn = true;
    const loggedInStore = mockStore(loggedInState);
    const expectedAction = { type: types.PUT_DISCUSSION_REQUEST };

    const boardWrapper = mount(<Board match={match} />, { context: { store: loggedInStore } });
    boardWrapper.find('.move-right').first().simulate('click');
    expect(loggedInStore.getActions()).toContainEqual(expectedAction);
  });
});