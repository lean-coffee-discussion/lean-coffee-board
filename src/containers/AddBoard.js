import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as boardActions from '../store/boards/actions';
import * as discussionActions from '../store/discussions/actions';
import * as userSelectors from '../store/user/reducer';
import * as boardsSelectors from '../store/boards/reducer';
import * as discussionsSelectors from '../store/discussions/reducer';
import * as types from '../store/actionTypes';
import ButtonWithText from '../components/ButtonWithText';
import TextInput from '../components/TextInput';
import './AddBoard.css';

class AddBoard extends Component {
  buttonClicked() {
    if (this.boardTitleInput && this.boardTitleInput.value && this.boardTitleInput.value.trim() !== '') {
      const baseIndex = this.props.nextDiscussionIndex;
      if (this.props.isLoggedIn) {
        this.props.dispatch(boardActions.createUserBoard(this.boardTitleInput.value, this.props.nextBoardIndex)).then((action) => {
          if (action.type === types.POST_BOARD_SUCCESS && action.payload && action.payload.id) {
            this.props.dispatch(discussionActions.createUserDiscussion(action.payload.id, 'To Discuss', baseIndex));
            this.props.dispatch(discussionActions.createUserDiscussion(action.payload.id, 'Discussing', baseIndex + 1));
            this.props.dispatch(discussionActions.createUserDiscussion(action.payload.id, 'Done', baseIndex + 2));
          }
        });
      } else {
        const action = boardActions.createBoard(this.boardTitleInput.value, this.props.nextBoardIndex);
        this.props.dispatch(action);
        this.props.dispatch(discussionActions.createDiscussion(action.payload.boardId, 'To Discuss', baseIndex));
        this.props.dispatch(discussionActions.createDiscussion(action.payload.boardId, 'Discussing', baseIndex + 1));
        this.props.dispatch(discussionActions.createDiscussion(action.payload.boardId, 'Done', baseIndex + 2));
      }
      this.boardTitleInput.value = '';
    }
  }

  render() {
    return (
      <div className="Add-Board">
        <TextInput className="Add-Board-input" value={this.boardTitle} onChange={() => { }} reference={(input) => this.boardTitleInput = input} placeholder="Board Name" />
        <ButtonWithText className="Add-Board-button" clickHandler={this.buttonClicked.bind(this)} text="Add New Board!" />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  nextBoardIndex: boardsSelectors.getNextBoardIndex(state),
  nextDiscussionIndex: discussionsSelectors.getNextDiscussionIndex(state),
  isLoggedIn: userSelectors.isUserLoggedIn(state)
});

export default connect(mapStateToProps)(AddBoard);