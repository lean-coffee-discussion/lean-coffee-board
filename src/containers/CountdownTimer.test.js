import React from 'react';
import { mount } from 'enzyme';
import CountdownTimer from './CountdownTimer';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import * as reducers from '../store/reducers';
import * as testState from '../store/testState.test';

var store, wrapper
let initialTime = 1487076708000;

jest.useFakeTimers();

describe('CountdownTimer', () => {
  beforeEach(() => {
    clearInterval.mockClear();
    store = createStore(combineReducers(reducers));
    wrapper = mount(<Provider store={store}><CountdownTimer time={testState.state.timers.time} /></Provider >);
    Date.now = jest.fn(() => initialTime);
  });

  it('should render a button with text', () => {
    expect(wrapper.find('button.Countdown-Timer-button').text()).toEqual('Start Timer');
  });

  it('should render the time', () => {
    expectTimerDisplayToEqual(testState.state.timers.time);
  });

  it('should render the time remaining', () => {
    expectTimerDisplayToEqual(testState.state.timers.time);
    clickTimerButton();

    runTimerTo(initialTime + 1000);
    expectTimerDisplayToEqual(testState.state.timers.time - 1000);

    runTimerTo(initialTime + 2000);
    expectTimerDisplayToEqual(testState.state.timers.time - 2000);
  });

  it('should change the button text when the button is clicked', () => {
    expect(wrapper.find('button.Countdown-Timer-button').text()).toEqual('Start Timer');
    clickTimerButton();
    expect(wrapper.find('button.Countdown-Timer-button').text()).toEqual('Stop Timer');
  });

  it('should stop the timer when the button is clicked again', () => {
    expectTimerDisplayToEqual(testState.state.timers.time);
    expect(wrapper.find('button.Countdown-Timer-button').text()).toEqual('Start Timer');

    clickTimerButton();
    expect(wrapper.find('button.Countdown-Timer-button').text()).toEqual('Stop Timer');

    runTimerTo(initialTime + 1000);
    expectTimerDisplayToEqual(testState.state.timers.time - 1000);

    runTimerTo(initialTime + 2000);
    expectTimerDisplayToEqual(testState.state.timers.time - 2000);

    clickTimerButton();

    runTimerTo(initialTime + 3000);
    expectTimerDisplayToEqual(testState.state.timers.time - 2000);
    expect(wrapper.find('button.Countdown-Timer-button').text()).toEqual('Start Timer');
  });

  it('should stop the timer when the time remaining hits zero', () => {
    expectTimerDisplayToEqual(testState.state.timers.time);

    clickTimerButton();

    runTimerTo(initialTime + testState.state.timers.time + 1000);
    expectTimerDisplayToEqual(0);

    expect(clearInterval.mock.calls.length).toBeGreaterThan(0);
  });

  it('should add 3 minutes to timer', () => {
    expectTimerDisplayToEqual(testState.state.timers.time);
    clickTimerButton();

    runTimerTo(initialTime + 1000);
    expectTimerDisplayToEqual(testState.state.timers.time - 1000);

    wrapper.find('button.Countdown-Timer-Add-3').simulate('click');
    runTimerTo(initialTime + 2000);
    expectTimerDisplayToEqual(testState.state.timers.time + (3 * 60 * 1000) - 2000);
  });

  it('should restart the timer if time is added.', () => {
    expectTimerDisplayToEqual(testState.state.timers.time);

    clickTimerButton();

    runTimerTo(initialTime + testState.state.timers.time + 1000);
    expectTimerDisplayToEqual(0);
    expect(clearInterval.mock.calls.length).toBeGreaterThan(0);

    wrapper.find('button.Countdown-Timer-Add-3').simulate('click');
    expectTimerDisplayToEqual((3 * 60 * 1000));

    runTimerTo(initialTime + testState.state.timers.time + 2000);
    expectTimerDisplayToEqual((3 * 60 * 1000 - 1000));
  });

  it('should reset the timer whenever stop timer is clicked.', () => {
    expectTimerDisplayToEqual(testState.state.timers.time);

    clickTimerButton();
    runTimerTo(initialTime + 1000);
    expectTimerDisplayToEqual(testState.state.timers.time - 1000);

    wrapper.find('button.Countdown-Timer-Add-3').simulate('click');
    expectTimerDisplayToEqual(testState.state.timers.time - 1000 + (3 * 60 * 1000));


    clickTimerButton();
    expectTimerDisplayToEqual(testState.state.timers.time - 1000 + (3 * 60 * 1000));
    clickTimerButton();
    expectTimerDisplayToEqual(testState.state.timers.time);
  });

  it('should add 1 minute to timer', () => {
    expectTimerDisplayToEqual(testState.state.timers.time);
    clickTimerButton();

    runTimerTo(initialTime + 1000);
    expectTimerDisplayToEqual(testState.state.timers.time - 1000);

    wrapper.find('button.Countdown-Timer-Add-1').simulate('click');
    runTimerTo(initialTime + 2000);
    expectTimerDisplayToEqual(testState.state.timers.time + (1 * 60 * 1000) - 2000);
  });

  function expectTimerDisplayToEqual(expectedTime) {
    expect(wrapper.find('.Countdown-Timer-display').text()).toEqual(getTimeString(expectedTime));
  }

  function clickTimerButton() {
    wrapper.find('button.Countdown-Timer-button').simulate('click');
  }

  function runTimerTo(epocTime) {
    Date.now = jest.fn(() => (epocTime));
    jest.runTimersToTime(1000);
  }

  function getTimeString(time) {
    const minutes = Math.floor(time / 60000);
    const seconds = (Math.floor(time % 60000) / 1000).toFixed(0);
    const secondString = (seconds < 10) ? `0${seconds}` : seconds;
    return `${minutes}:${secondString}`;
  }
});