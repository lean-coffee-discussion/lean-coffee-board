import React, { Component } from 'react';
import { connect } from 'react-redux';
import ListTopics from './ListTopics';

class Discussion extends Component {
  render() {
    return (
      <div>
        <div className='discussion-title'>
          {this.props.title}
        </div>
        <ListTopics
          discussionId={this.props.discussionId}
          moveTopicHandler={(topicId, direction) => { this.props.moveTopicHandler(this.props.discussionId, topicId, direction) }} />
      </div>
    );
  }
}
export default connect()(Discussion);