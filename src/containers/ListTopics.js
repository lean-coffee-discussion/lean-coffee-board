import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import './ListTopics.css';
import ListView from '../components/ListView';
import * as topicsSelectors from '../store/topics/reducer';
import * as topicsActions from '../store/topics/actions';
import * as discussionsSelectors from '../store/discussions/reducer';
import * as constants from '../constants';
import ic_keyboard_arrow_left_black_24px from '../icons/ic_keyboard_arrow_left_black_24px.svg';
import ic_keyboard_arrow_right_black_24px from '../icons/ic_keyboard_arrow_right_black_24px.svg';
import ic_add_circle_black_24px from '../icons/ic_add_circle_black_24px.svg';

class ListTopics extends Component {

  render() {
    return (
      <div className="List-Topics">
        <ListView
          className="List-Topics-list"
          rowsIdArray={this.props.rowsIdArray}
          rowsById={this.props.rowsById}
          renderRow={this.renderRow.bind(this)} />
      </div>
    )
  }

  renderRow(row) {
    return (
      <div>
        <button className="move-left" onClick={() => { this.props.moveTopicHandler(row.id, constants.LEFT_DIRECTION) }}><img src={ic_keyboard_arrow_left_black_24px} alt="move left" /></button>
        <div>{row.title}</div>
        <div className="centered-container">
          <button className="dot-vote" onClick={this.dotVoteTopic.bind(this, row.id)}><img src={ic_add_circle_black_24px} alt="dot vote" />{row.votes || 0}</button>
        </div>
        <button className="move-right" onClick={() => { this.props.moveTopicHandler(row.id, constants.RIGHT_DIRECTION) }}><img src={ic_keyboard_arrow_right_black_24px} alt="move right" /></button>
      </div >
    )
  }

  dotVoteTopic(topicId) {
    this.props.dispatch(topicsActions.upVoteTopic(topicId));
  }

}

const mapStateToProps = (state, props) => ({
  rowsById: topicsSelectors.getTopicsById(state),
  rowsIdArray: _.intersection(topicsSelectors.getTopicsIdArray(state), discussionsSelectors.getDiscussionTopics(state, props))
});

export default connect(mapStateToProps)(ListTopics);