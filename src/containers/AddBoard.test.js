import React from 'react';
import { mount } from 'enzyme';
import AddBoard from './AddBoard';
import configureMockStore from 'redux-mock-store';
import { apiMiddleware } from 'redux-api-middleware';
import thunk from 'redux-thunk';
import * as testState from '../store/testState.test';
import * as types from '../store/actionTypes';

const middleware = [apiMiddleware, thunk];
let store;

const mockResponse = (status, statusText, response) => {
  return new window.Response(response, {
    status: status,
    statusText: statusText,
    headers: {
      'Content-type': 'application/json'
    }
  });
};

function successResponse(response) {
  return () => Promise.resolve(mockResponse(200, null, JSON.stringify(response)));
};

describe('AddBoardButton', () => {
  beforeEach(() => {
    store = configureMockStore()(testState.state);
  });

  it('should render a button with text', () => {
    const wrapper = mount(<AddBoard />, { context: { store } });
    expect(wrapper.find('ButtonWithText').text()).toEqual('Add New Board!');
  });

  it('should render a text input field', () => {
    const wrapper = mount(<AddBoard />, { context: { store } });
    expect(wrapper.find('TextInput').exists()).toBeTruthy();
  });

  it('should add a board with a given name', () => {
    const testTitle = 'A Test Board Title!';
    const wrapper = mount(<AddBoard />, { context: { store } });
    wrapper.find('input').instance().value = testTitle;
    wrapper.find('button').simulate('click');
    expect(store.getActions().length).toBeGreaterThan(0);
    expect(store.getActions()[0].payload.title).toBe(testTitle);
    expect(store.getActions()[0].payload.index).toBe(100);
  });

  it('should add default discussions to the board when a board is created', () => {
    const testTitle = 'A Test Board Title!';
    const wrapper = mount(<AddBoard />, { context: { store } });
    wrapper.find('input').instance().value = testTitle;
    wrapper.find('button').simulate('click');
    expect(store.getActions().length).toEqual(4);
    expect(store.getActions()[0].payload.title).toBe(testTitle);
    expect(store.getActions()[0].payload.index).toBe(testState.maxBoardIndex + 1);
    expect(store.getActions()[1].payload.title).toBe('To Discuss');
    expect(store.getActions()[1].payload.index).toBe(testState.maxDiscussionIndex + 1);
    expect(store.getActions()[2].payload.title).toBe('Discussing');
    expect(store.getActions()[2].payload.index).toBe(testState.maxDiscussionIndex + 2);
    expect(store.getActions()[3].payload.title).toBe('Done');
    expect(store.getActions()[3].payload.index).toBe(testState.maxDiscussionIndex + 3);
  });

  it('should not add a board with an empty name', () => {
    const emptyTitle = '  ';
    const wrapper = mount(<AddBoard />, { context: { store } });
    wrapper.find('input').instance().value = emptyTitle;
    wrapper.find('button').simulate('click');
    expect(store.getActions().length).toEqual(0);
  });

  it('should not add a board with an undefined name', () => {
    const wrapper = mount(<AddBoard />, { context: { store } });
    wrapper.find('button').simulate('click');
    expect(store.getActions().length).toEqual(0);
  });

  it('should clear the text field once the new board is created.', () => {
    const testTitle = 'A Test Board Title!';
    const wrapper = mount(<AddBoard />, { context: { store } });
    const textInput = wrapper.find('input');
    textInput.instance().value = testTitle;
    wrapper.find('input').simulate('change', { target: { value: testTitle } });
    wrapper.find('button').simulate('click');
    expect(store.getActions().length).toBeGreaterThan(0);
    expect(store.getActions()[0].payload.title).toBe(testTitle);
    expect(textInput.instance().value).toEqual('');
  });

  it('should add a board with a given name when the user is logged in', () => {
    var loggedInState = testState.state;
    loggedInState.user.token = 'abc';
    loggedInState.user.isLoggedIn = true;
    const testTitle = 'A Test Board Title!';
    const expectedBody = { title: testTitle, index: 100 };
    store = configureMockStore(middleware)(loggedInState);
    const wrapper = mount(<AddBoard />, { context: { store } });
    const mockFetch = jest.fn().mockImplementation(successResponse({}));
    window.fetch = mockFetch;
    wrapper.find('input').instance().value = testTitle;

    wrapper.find('button').simulate('click');

    expect(store.getActions().length).toEqual(1);
    expect(store.getActions()[0].type).toBe(types.POST_BOARD_REQUEST);
    expect(mockFetch.mock.calls[0][1].body).toBe(JSON.stringify(expectedBody));
  });

});