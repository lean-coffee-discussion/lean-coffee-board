import React, { Component } from 'react';
import { connect } from 'react-redux';
import ListView from '../components/ListView';
import * as boardActions from '../store/boards/actions';
import * as boardsSelectors from '../store/boards/reducer';
import * as userSelectors from '../store/user/reducer';
import './ListBoard.css';
import { action as toggleMenu } from 'redux-burger-menu';
import { Link } from 'react-router-dom';

class ListBoards extends Component {

  componentWillReceiveProps(nextProps) {
    if (this.props && this.props.isLoggedIn === false && nextProps.isLoggedIn === true) {
      this.props.dispatch(boardActions.getBoards());
    }
  }

  linkClicked(row, event) {
    this.props.dispatch(toggleMenu(false));
  }

  render = () => (
    <div className="List-Board" >
      <ListView
        className="List-Board-list"
        rowsIdArray={this.props.rowsIdArray}
        rowsById={this.props.rowsById}
        renderRow={this.renderRow.bind(this)} />
    </div>
  )

  renderRow(row) {
    return (
      <div>
        <Link onClick={this.linkClicked.bind(this, row)} className="List-Board-link" to={`/Boards/${row.id}`}>{row.title}</Link>
      </div>
    )
  }

}

const mapStateToProps = state => ({
  rowsById: boardsSelectors.getBoardsById(state),
  rowsIdArray: boardsSelectors.getBoardIdArray(state),
  isLoggedIn: userSelectors.isUserLoggedIn(state)
});

export default connect(mapStateToProps)(ListBoards);
