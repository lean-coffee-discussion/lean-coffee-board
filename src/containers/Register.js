import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as userActions from '../store/user/actions';
import * as userSelectors from '../store/user/reducer';
import { LocalForm, Control } from 'react-redux-form';
import { action as toggleMenu } from 'redux-burger-menu';
import * as types from '../store/actionTypes';
import './Register.css';

class Register extends Component {
  state = {
    redirectToRoot: false
  };

  register = (values) => {
    this.props.dispatch(userActions.register(values.username, values.password, values.confirm))
      .then((result) => {
        if (result.type === types.USER_REGISTER_SUCCESS) {
          this.props.dispatch(toggleMenu(true));
          this.setState({ redirectToRoot: true });
        }
      });
  };

  render() {
    const { redirectToRoot } = this.state;

    if (redirectToRoot) {
      return <Redirect to="/" />;
    }

    return (
      <div className="Register-form">
        <LocalForm onSubmit={(values) => this.register(values)}>
          <div className="Register-form input">Email: <Control.text model=".username" /></div>
          <div className="Register-form input">Password: <Control.text type="password" model=".password" /></div>
          <div className="Register-form input">Confirm Password: <Control.text type="password" model=".confirm" /></div>
          <div className="Register-form submit"><button type="submit">Sign Up!</button></div>
        </LocalForm>
        <div className="error-message">{this.props.registerStatusText}</div>
      </div >
    );
  }
}

const mapStateToProps = (state) => ({
  registerStatusText: userSelectors.getUserStatusText(state)
});

export default connect(mapStateToProps)(Register);