import React, { Component } from 'react';
import { connect } from 'react-redux';
import './AddTopic.css';
import TextInput from '../components/TextInput';
import ButtonWithText from '../components/ButtonWithText';
import { createUserTopic } from '../store/topics/actions';
import * as userSelectors from '../store/user/reducer';
import * as topicsSelectors from '../store/topics/reducer';
import _ from 'lodash';

class AddTopic extends Component {
  buttonClicked() {
    if (_.get(this, 'topicTitleInput.value')) {
      this.props.dispatch(createUserTopic(this.props.discussionId, this.topicTitleInput.value.trim(), this.props.nextTopicIndex));
      this.topicTitleInput.value = '';
    }
  }

  render() {
    return (
      <div>
        <div>
          <TextInput
            className="Add-Topic-input"
            onChange={() => { }} reference={(input) => this.topicTitleInput = input}
            placeholder="What would you like to discuss?" />
        </div>
        <div>
          <ButtonWithText
            className="Add-Topic-button"
            clickHandler={this.buttonClicked.bind(this)}
            text="Add Topic">
          </ButtonWithText>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  nextTopicIndex: topicsSelectors.getNextTopicIndex(state),
  isLoggedIn: userSelectors.isUserLoggedIn(state)
});

export default connect(mapStateToProps)(AddTopic); 