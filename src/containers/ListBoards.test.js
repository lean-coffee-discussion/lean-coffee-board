import React from 'react';
import { shallow } from 'enzyme';
import ListBoards from './ListBoards';
import configureMockStore from 'redux-mock-store';
import { apiMiddleware } from 'redux-api-middleware';
import * as testState from '../store/testState.test';
import * as types from '../store/actionTypes';

const middleware = [apiMiddleware];
const mockStore = configureMockStore(middleware);
let store;

describe('ListBoards', () => {

  beforeEach(() => {
    store = mockStore(testState.state);
  });

  it('should render an empty list', () => {
    store = configureMockStore()({ user: testState.state.user, boards: { byId: {} } });
    const wrapper = shallow(<ListBoards />, { context: { store } });
    const listWrapper = wrapper.dive().find('ListView');

    expect(wrapper.dive().find('ListView').length).toEqual(1);
    expect(listWrapper.find('li').length).toEqual(0);
  });

  it('should render a list with 2 items', () => {
    const wrapper = shallow(<ListBoards />, { context: { store } });
    const listWrapper = wrapper.dive().find('ListView').dive();
    expect(listWrapper.find('ul').length).toEqual(1);
    expect(listWrapper.find('li').length).toEqual(2);
    expect(listWrapper.find('Link').length).toEqual(2);
  });

  it('should dispatch a toggle menu event when the link is clicked', () => {
    const wrapper = shallow(<ListBoards />, { context: { store } });
    const listWrapper = wrapper.dive().find('ListView').dive();

    listWrapper.find('Link').at(0).simulate('click');

    expect(store.getActions().length).toEqual(1);
    expect(store.getActions()[0].type).toEqual('TOGGLE_MENU');
  });
});