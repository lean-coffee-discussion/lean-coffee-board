import React, { Component } from 'react';
import { connect } from 'react-redux';
import Menu from '../components/Menu';
import AddBoard from './AddBoard'
import './BoardsMenu.css';
import ListBoards from './ListBoards';

class BoardsMenu extends Component {
  showSettings(event) {
    event.preventDefault();
  }

  render() {
    return (
      <Menu>
        <AddBoard />
        <ListBoards />
      </Menu>
    );
  }
}

export default connect()(BoardsMenu); 