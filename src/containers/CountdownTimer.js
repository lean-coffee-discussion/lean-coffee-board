import React, { Component } from 'react';
import { connect } from 'react-redux';
import ButtonWithText from '../components/ButtonWithText';
import * as timersActions from '../store/timers/actions';
import * as timersSelectors from '../store/timers/reducer';
import './CountdownTimer.css';

class CountdownTimer extends Component {
  componentDidMount() {
    this.props.dispatch(timersActions.setTime(this.props.time));
  }

  buttonClicked() {
    if (this.timer) {
      this.stopTimer(this.timer);
    } else {
      this.props.dispatch(timersActions.setTime(this.props.time));
      this.timer = this.startTimer();
    }
  }

  startTimer() {
    this.props.dispatch(timersActions.startTimer(Date.now()));
    return setInterval(this.tick.bind(this), 1000);
  }

  stopTimer(timer) {
    clearInterval(timer);
    this.timer = null;
    this.props.dispatch(timersActions.toggleTimer(false));
    if (this.props.timerRemaining === 0) {
      this.props.dispatch(timersActions.setTime(0));
    }
  }

  tick() {
    this.props.dispatch(timersActions.setElapsed(Date.now()));
    if (this.props.timerRemaining <= 0) {
      this.stopTimer(this.timer);
    }
  }

  formatTime(time) {
    const minutes = Math.floor(time / 60000);
    const seconds = (Math.floor(time % 60000) / 1000).toFixed(0);
    const secondString = (seconds < 10) ? `0${seconds}` : seconds;
    return `${minutes}:${secondString}`;
  }

  addTime(seconds) {
    this.props.dispatch(timersActions.addTime(seconds));
    if (!this.props.isRunning) {
      this.timer = this.startTimer();
    }
  }

  render = () => {
    return (
      <div>
        <div className="Countdown-Timer-display">{this.formatTime(this.props.timerRemaining)}</div>
        <ButtonWithText
          className="Countdown-Timer-button"
          clickHandler={this.buttonClicked.bind(this)}
          text={this.props.isRunning ? "Stop Timer" : "Start Timer"} />
        <ButtonWithText
          className="Countdown-Timer-Add-3"
          clickHandler={this.addTime.bind(this, 180)} text="+3 min" />
        <ButtonWithText
          className="Countdown-Timer-Add-1"
          clickHandler={this.addTime.bind(this, 60)} text="+1 min" />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  timerRemaining: timersSelectors.getRemaining(state),
  isRunning: timersSelectors.isRunning(state)
});

export default connect(mapStateToProps)(CountdownTimer);