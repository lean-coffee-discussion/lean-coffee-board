import React from 'react';
import { mount } from 'enzyme';
import AddTopic from './AddTopic';
import configureMockStore from 'redux-mock-store';
import { apiMiddleware } from 'redux-api-middleware';
import * as testState from '../store/testState.test';
import * as types from '../store/actionTypes';

const middleware = [apiMiddleware];
let store;

const mockResponse = (status, statusText, response) => {
  return new window.Response(response, {
    status: status,
    statusText: statusText,
    headers: {
      'Content-type': 'application/json'
    }
  });
};

function successResponse(response) {
  return () => Promise.resolve(mockResponse(200, null, JSON.stringify(response)));
};

describe('AddTopic', () => {
  beforeEach(() => {
    store = configureMockStore(middleware)(testState.state);
  });

  it('should render a AddTopic component', () => {
    const wrapper = mount(<AddTopic />, { context: { store } });
    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render a text input field', () => {
    const wrapper = mount(<AddTopic />, { context: { store } });
    expect(wrapper.find('TextInput').exists()).toBeTruthy();
  });

  it('should render a button', () => {
    const wrapper = mount(<AddTopic />, { context: { store } });
    expect(wrapper.find('ButtonWithText').exists()).toBeTruthy();
  });

  it('should not create a topic with an empty name', () => {
    const wrapper = mount(<AddTopic />, { context: { store } });
    wrapper.find('button').simulate('click');
    expect(store.getActions().length).toEqual(0);
  });

  it('should say add topic on the button', () => {
    const wrapper = mount(<AddTopic />, { context: { store } });
    const button = wrapper.find('button')
    expect(button.props().children).toEqual('Add Topic');
  });

  it('should add a topic with a given name when the user is logged in', () => {
    var loggedInState = testState.state;
    loggedInState.user.token = 'abc';
    loggedInState.user.isLoggedIn = true;
    const testTitle = 'A Test Board Title!';
    const expectedBody = { title: testTitle, index: testState.maxTopicIndex + 1 };
    store = configureMockStore(middleware)(loggedInState);
    const mockFetch = jest.fn().mockImplementation(successResponse({}));
    window.fetch = mockFetch;
    const wrapper = mount(<AddTopic />, { context: { store } });
    wrapper.find('input').instance().value = testTitle;

    wrapper.find('button').simulate('click');
    expect(store.getActions().length).toEqual(1);
    expect(store.getActions()[0].type).toBe(types.POST_TOPIC_REQUEST);
    expect(mockFetch.mock.calls[0][1].body).toBe(JSON.stringify(expectedBody));
  });
});