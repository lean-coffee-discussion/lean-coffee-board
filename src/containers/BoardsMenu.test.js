import React from 'react';
import { shallow } from 'enzyme';
import BoardsMenu from './BoardsMenu';
import configureMockStore from 'redux-mock-store';
import * as testState from '../store/testState.test';

var store;

describe('BoardsMenu', () => {
  beforeEach(() => {
    store = configureMockStore()(testState.state);
  });

  it('should render a slide BoardsMenu', () => {
    const wrapper = shallow(<BoardsMenu />, { context: { store } });
    expect(wrapper.exists).toBeTruthy();
    expect(wrapper.find('BoardsMenu').length).toEqual(1);
  });

  it('should contain AddBoard container', () => {
    const wrapper = shallow(<BoardsMenu />, { context: { store } });
    expect(wrapper.exists).toBeTruthy();
    expect(wrapper.dive().find('Connect(AddBoard)').length).toEqual(1);
  });

  it('should contain list boards container', () => {
    const wrapper = shallow(<BoardsMenu />, { context: { store } });
    expect(wrapper.exists()).toBeTruthy();
    expect(wrapper.dive().find('Connect(ListBoards)').length).toEqual(1);
  })
});