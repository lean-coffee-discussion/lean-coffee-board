import React from 'react';
import { mount } from 'enzyme';
import ListTopics from './ListTopics';
import configureMockStore from 'redux-mock-store';
import { apiMiddleware } from 'redux-api-middleware';
import * as testState from '../store/testState.test';
import * as constants from '../constants';
import * as types from '../store/actionTypes';

const middleware = [apiMiddleware];
const mockStore = configureMockStore(middleware);
let store;
let wrapper;
const numberOfTopicsInDiscussion = testState.state.discussions.byId[testState.firstDiscussionId].topics.length;
const buttonAction = jest.fn();

describe('ListTopics', () => {
  beforeEach(() => {
    store = mockStore(testState.state);
    wrapper = mount(<ListTopics discussionId={testState.firstDiscussionId} moveTopicHandler={buttonAction} />, { context: { store } });
    buttonAction.mockClear();
  });

  it('should render a ListTopics component', () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it('should contain a list of topics', () => {
    expect(wrapper.find('li').length).toBe(numberOfTopicsInDiscussion);
  });

  it('should contain a button for moving the card to the right', () => {
    expect(wrapper.find('.move-right').length).toBe(numberOfTopicsInDiscussion);
  });

  it('should call the correct function when pressing the move right button', () => {
    wrapper.find('.move-right').first().simulate('click');
    expect(buttonAction.mock.calls.length).toBe(1);
    expect(buttonAction.mock.calls[0][0]).toBe(testState.firstTopicId);
    expect(buttonAction.mock.calls[0][1]).toBe(constants.RIGHT_DIRECTION);

    wrapper.find('.move-right').first().simulate('click');
    expect(buttonAction.mock.calls.length).toBe(2);
    expect(buttonAction.mock.calls[1][0]).toBe(testState.firstTopicId);
    expect(buttonAction.mock.calls[1][1]).toBe(constants.RIGHT_DIRECTION);
  });

  it('should call the correct function when pressing the move left button', () => {
    wrapper.find('.move-left').first().simulate('click');
    expect(buttonAction.mock.calls.length).toBe(1);
    expect(buttonAction.mock.calls[0][0]).toBe(testState.firstTopicId);
    expect(buttonAction.mock.calls[0][1]).toBe(constants.LEFT_DIRECTION);

    wrapper.find('.move-left').first().simulate('click');
    expect(buttonAction.mock.calls.length).toBe(2);
    expect(buttonAction.mock.calls[1][0]).toBe(testState.firstTopicId);
    expect(buttonAction.mock.calls[1][1]).toBe(constants.LEFT_DIRECTION);
  });

  it('should contain a button for dot-voting the topic', () => {
    expect(wrapper.find('.dot-vote').length).toBe(numberOfTopicsInDiscussion);
  });

  it('should call the correct function when pressing the dot-vote button', () => {
    wrapper.find('.dot-vote').first().simulate('click');
    expect(store.getActions()[0]).toEqual({ type: types.POST_VOTE_REQUEST });
  });

  it('should contain the number of dot votes for a topic', () => {
    expect(wrapper.find('.dot-vote').first().text()).toBe(testState.firstTopicVotes.toString());
  });
});