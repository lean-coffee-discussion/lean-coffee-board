import React from 'react';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import Register from './Register';
import { apiMiddleware } from 'redux-api-middleware';
import thunk from 'redux-thunk';
import * as testState from '../store/testState.test';

const middleware = [apiMiddleware, thunk];
const mockStore = configureMockStore(middleware);
const props = {
  location: {
    state: {
      from: {
        pathname: 'testPathName'
      }
    }
  }
}

let store;
let wrapper;

describe('Register', () => {
  beforeEach(() => {
    store = mockStore(testState.state);
    wrapper = mount(<Register {...props} />, { context: { store } });
  });

  it('should render a Register component', () => {
    expect(wrapper.exists()).toBeTruthy();
  });
});