import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import * as userActions from '../store/user/actions';
import * as userSelectors from '../store/user/reducer';
import { LocalForm, Control } from 'react-redux-form';
import { action as toggleMenu } from 'redux-burger-menu';
import * as types from '../store/actionTypes';
import './Login.css';

class Login extends Component {
  state = {
    redirectToReferrer: false
  };

  login = (values) => {
    this.props.dispatch(userActions.login(values.username, values.password))
      .then((result) => {
        if (result.type === types.USER_LOGIN_SUCCESS) {
          this.props.dispatch(toggleMenu(true));
          this.setState({ redirectToReferrer: true });
        }
      });
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }

    return (
      <div className="Login-form">
        <LocalForm onSubmit={(values) => this.login(values)}>
          <div className="Login-form input">Email: <Control.text model=".username" /></div>
          <div className="Login-form input">Password: <Control.text type="password" model=".password" /></div>
          <div className="Login-form submit"><button type="submit">Login</button></div>
        </LocalForm>
        <div className="Login-form text"><Link to="/register">Click Here to Register!</Link></div>
        <div className="error-message">{this.props.registerStatusText}</div>
      </div >
    );
  }
}

const mapStateToProps = (state) => ({
  registerStatusText: userSelectors.getUserStatusText(state)
});

export default connect(mapStateToProps)(Login);