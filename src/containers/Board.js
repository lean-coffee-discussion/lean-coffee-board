import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Board.css';
import AddTopic from './AddTopic';
import _ from 'lodash';
import CountdownTimer from './CountdownTimer';
import ColumnsView from '../components/ColumnsView';
import Discussion from './Discussion';
import * as discussionsActions from '../store/discussions/actions';
import * as discussionsSelectors from '../store/discussions/reducer';
import * as boardsActions from '../store/boards/actions';
import * as boardsSelectors from '../store/boards/reducer';
import * as userSelectors from '../store/user/reducer';

class Board extends Component {

  componentWillReceiveProps(nextProps) {
    const prevBoardId = _.get(this, 'props.match.params.boardId');
    const nextBoardId = _.get(nextProps, 'match.params.boardId');
    if (prevBoardId && nextBoardId && nextBoardId !== prevBoardId) {
      this.props.dispatch(boardsActions.getBoard(nextBoardId));
    }
  }

  componentDidMount() {
    const boardId = _.get(this, 'props.match.params.boardId');
    if (boardId) {
      this.props.dispatch(boardsActions.getBoard(boardId));
    }
  }

  render() {
    const params = _.get(this, 'props.match.params');
    if (params && params.boardId && this.props.columnsIdArray && this.props.columnsIdArray.length > 0) {
      const firstDiscussionId = this.props.columnsIdArray[0];
      return (
        <div>
          <div className="timer"><CountdownTimer time={300000} /></div>
          <div className="add-topic"><AddTopic discussionId={firstDiscussionId} /></div>
          <div className="board">
            <ColumnsView
              columnsIdArray={this.props.columnsIdArray}
              columnsById={this.props.columnsById}
              renderColumn={this.renderDiscussion.bind(this)} />
          </div>
        </div>
      );
    } else {
      return (<div>{this.props.boardStatusText}</div>);
    }
  }

  renderDiscussion(discussion) {
    return (
      <div>
        <Discussion
          discussionId={discussion.id}
          title={discussion.title}
          moveTopicHandler={this.moveTopic.bind(this)} />
      </div>
    );
  }

  moveTopic(sourceDiscussionId, topicId, direction) {
    const destIndex = this.props.columnsIdArray.indexOf(sourceDiscussionId) + direction;
    const destDiscussionId = this.props.columnsIdArray[destIndex];
    if (destDiscussionId) {
      if (this.props.isLoggedIn) {
        const params = _.get(this, 'props.match.params');
        if (params && params.boardId) {
          this.props.dispatch(discussionsActions.moveUserTopic(params.boardId, sourceDiscussionId, destDiscussionId, topicId));
        }
      } else {
        this.props.dispatch(discussionsActions.moveTopic(sourceDiscussionId, destDiscussionId, topicId));
      }
    }
  }
}

const mapStateToProps = (state, props) => ({
  columnsIdArray: _.intersection(discussionsSelectors.getDiscussionsIdArray(state), boardsSelectors.getBoardDiscussions(state, props)),
  columnsById: discussionsSelectors.getDiscussionsById(state),
  boardStatusText: boardsSelectors.getBoardStatusText(state),
  isLoggedIn: userSelectors.isUserLoggedIn(state)
});

export default connect(mapStateToProps)(Board); 