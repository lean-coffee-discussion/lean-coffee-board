import React from 'react';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import Login from './Login';
import * as testState from '../store/testState.test';

const history = createHistory();
const mockStore = configureMockStore();

var store;
var wrapper;
var props = {
  location: {
    state: {
      from: {
        pathname: 'testPathName'
      }
    }
  }
}
describe('Login', () => {
  beforeEach(() => {
    store = mockStore(testState.state);
    wrapper = mount(<ConnectedRouter history={history}><Login {...props} /></ConnectedRouter>, { context: { store } });
  });

  it('should render a Login component', () => {
    expect(wrapper.exists()).toBeTruthy();
  });
});