import React from 'react';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import { apiMiddleware } from 'redux-api-middleware';
import Discussion from './Discussion';
import * as testState from '../store/testState.test';
import * as constants from '../constants';

const testTitle = 'A Discussion Title';
let store;
const middleware = [apiMiddleware];

var wrapper;
const mockStore = configureMockStore(middleware);
const moveTopicHandler = jest.fn();

describe('Discussion Container', () => {
  beforeEach(() => {
    store = mockStore(testState.state);
    wrapper = shallow(<Discussion title={testTitle} discussionId={testState.firstDiscussionId} moveTopicHandler={moveTopicHandler} />, { context: { store } });
    moveTopicHandler.mockClear();
  });

  it('should render a list of topics', () => {
    expect(wrapper.dive().find('Connect(ListTopics)').exists()).toBeTruthy();
  });

  it('should render the discussion title', () => {
    const titleWrapper = wrapper.dive().find('.discussion-title');
    expect(titleWrapper.length).toBe(1);
    expect(titleWrapper.text()).toBe(testTitle);
  });

  it('should pass discussion id down to the list topics container', () => {
    expect(wrapper.dive().find('Connect(ListTopics)').dive().props().discussionId).toEqual(testState.firstDiscussionId);
  });

  it('should call move right with the correct parameters when the button is clicked', () => {
    const discussionWrapper = wrapper.dive().find('Connect(ListTopics)').dive().dive().find('ListView').dive();
    discussionWrapper.find('.move-right').first().simulate('click');
    expect(moveTopicHandler.mock.calls.length).toBe(1);
    expect(moveTopicHandler.mock.calls[0][0]).toEqual(testState.firstDiscussionId);
    expect(moveTopicHandler.mock.calls[0][1]).toEqual(testState.firstTopicId);
    expect(moveTopicHandler.mock.calls[0][2]).toEqual(constants.RIGHT_DIRECTION);
  });

  it('should call move left with the correct parameters when the button is clicked', () => {
    const discussionWrapper = wrapper.dive().find('Connect(ListTopics)').dive().dive().find('ListView').dive();
    discussionWrapper.find('.move-left').first().simulate('click');
    expect(moveTopicHandler.mock.calls.length).toBe(1);
    expect(moveTopicHandler.mock.calls[0][0]).toEqual(testState.firstDiscussionId);
    expect(moveTopicHandler.mock.calls[0][1]).toEqual(testState.firstTopicId);
    expect(moveTopicHandler.mock.calls[0][2]).toEqual(constants.LEFT_DIRECTION);
  });
});