import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import logo from './logo.svg';
import BoardsMenu from './containers/BoardsMenu';
import Board from './containers/Board';
import Login from './containers/Login';
import Register from './containers/Register';
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Lean Coffee Board</h1>
          <Link to="/login" className="App-login-link">
            <button>Login</button>
          </Link>
        </header>
        <div id="content">
          <BoardsMenu pageWrapId={"board"} outerContainerId={"content"} />
          <div id="board">
            <Route path="/boards/:boardId" component={Board} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
