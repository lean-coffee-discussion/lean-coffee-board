export const firstBoardId = 'first-board-id';
export const secondBoardId = 'second-board-id';
export const firstDiscussionId = 'first-discussion-id';
export const secondDiscussionId = 'second-discussion-id';
export const lastDiscussionId = 'last-discussion-id'
export const firstTopicId = 'first-topic-id';
export const lastTopicId = 'last-topic-id';
export const maxBoardIndex = 99;
export const maxDiscussionIndex = 109;
export const maxTopicIndex = 12;
export const firstTopicVotes = 49;


export const state = {
  user: {},
  boards: {
    byId: {
      [firstBoardId]: {
        id: firstBoardId,
        title: 'An Existing Board',
        index: 98,
        discussions: [firstDiscussionId, lastDiscussionId, secondDiscussionId]
      },
      [secondBoardId]: {
        id: secondBoardId,
        index: maxBoardIndex,
        title: 'New Board Title',
        discussions: ['second-board-discussion', 'another-second-board-discussion', 'last-second-board-discussion']
      }
    },
    maxIndex: maxBoardIndex
  },
  discussions: {
    byId: {
      [firstDiscussionId]: {
        id: firstDiscussionId,
        title: 'To Discuss',
        index: 107,
        topics: ['987-654', firstTopicId]
      },
      'last-second-board-discussion': {
        id: 'last-second-board-discussion',
        title: 'To Discuss',
        index: 107,
        topics: []
      },
      [lastDiscussionId]: {
        id: lastDiscussionId,
        title: 'Done',
        index: maxDiscussionIndex,
        topics: [lastTopicId]
      },
      'second-board-discussion': {
        id: 'second-board-discussion',
        title: 'Done',
        index: maxDiscussionIndex,
        topics: []
      },
      'another-second-board-discussion': {
        id: 'another-second-board-discussion',
        title: 'Discussing',
        index: 108,
        topics: []
      },
      [secondDiscussionId]: {
        id: secondDiscussionId,
        title: 'Discussing',
        index: 108,
        topics: []
      }
    },
    maxIndex: maxDiscussionIndex
  },
  topics: {
    byId: {
      [firstTopicId]: {
        id: firstTopicId,
        title: 'A very interesting topic',
        index: 10,
        votes: firstTopicVotes
      },
      [lastTopicId]: {
        id: lastTopicId,
        title: 'The Very Last Topic',
        index: maxTopicIndex
      },
      '987-654': {
        id: '987-654',
        title: 'Another Interesting Topic',
        index: 11
      }
    },
    maxIndex: maxTopicIndex
  },
  timers: {
    initial: 0,
    elapsed: 0,
    time: 180000,
    isRunning: false
  },
  burgerMenu: {}
};

it('should define a test state', () => {
  expect(state).toBeDefined();
});