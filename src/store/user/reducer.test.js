import reduce, { isUserLoggedIn, getUserStatusText } from './reducer';
import * as types from '../actionTypes';
import { LOCATION_CHANGE } from 'react-router-redux';

describe('User Reducer', () => {
  it('should return the initial state', () => {
    expect(reduce(undefined, {})).toEqual({ isLoggedIn: false })
  });

  it('should set the authentication token into state', () => {
    const testToken = '123456';
    const expectedState = { token: testToken, isLoggedIn: true };
    expect(reduce(undefined, { type: types.USER_LOGIN_SUCCESS, payload: { token: testToken } })).toEqual(expectedState);
  });

  it('should reset the authentication token into state if the state exists already', () => {
    const testToken = '123456';
    const expectedState = { token: testToken, isLoggedIn: true };
    expect(reduce({ token: 'abcdef' }, { type: types.USER_LOGIN_SUCCESS, payload: { token: testToken } })).toEqual(expectedState);
  });

  it('should set the authentication token into state if the user registers successfully', () => {
    const testToken = '123456';
    const expectedState = { token: testToken, isLoggedIn: true };
    expect(reduce({}, { type: types.USER_REGISTER_SUCCESS, payload: { token: testToken } })).toEqual(expectedState);
  });

  it('should set user login failure status and message into state', () => {
    const expectedState = {
      error: {
        status: 400,
        message: '400 - Not Authorized'
      },
      isLoggedIn: false,
      token: ''
    };
    expect(reduce({}, {
      type: types.USER_LOGIN_FAILURE, payload: {
        status: 400,
        message: '400 - Not Authorized'
      }
    })).toEqual(expectedState);
  });

  it('should set user registration failure status and message into state', () => {
    const expectedState = {
      error: {
        status: 400,
        message: '400 - Not Authorized'
      },
      isLoggedIn: false,
      token: ''
    };
    expect(reduce({}, {
      type: types.USER_REGISTER_FAILURE, payload: {
        status: 400,
        message: '400 - Not Authorized'
      }
    })).toEqual(expectedState);
  });

  it('should reset error when user attempts login or registration', () => {
    const expectedState = {
      error: undefined
    }
    expect(reduce({ error: { status: 400, message: 'Previous Error Text' } }, { type: types.USER_REGISTER_REQUEST })).toEqual(expectedState);
    expect(reduce({ error: { status: 400, message: 'Previous Error Text' } }, { type: types.USER_LOGIN_REQUEST })).toEqual(expectedState);
  });

  it('should reset error when location changes from the login or registration page', () => {
    const expectedState = {
      error: undefined
    }
    expect(reduce({ error: { status: 400, message: 'Previous Error Text' } }, { type: LOCATION_CHANGE })).toEqual(expectedState);
  });

  it('should set JWT token into session storage when user logs in', () => {
    global.sessionStorage.setItem.mockClear();
    const testToken = '123456';
    const expectedState = { token: testToken, isLoggedIn: true };
    expect(reduce({}, { type: types.USER_REGISTER_SUCCESS, payload: { token: testToken } })).toEqual(expectedState);
    expect(global.sessionStorage.setItem.mock.calls.length).toBe(1);
    expect(global.sessionStorage.setItem.mock.calls[0]).toEqual(['jwtToken', testToken]);
  });

  it('should set an empty JWT token into session storage when user fails to log in', () => {
    global.sessionStorage.setItem.mockClear();
    const expectedState = {
      error: {},
      isLoggedIn: false,
      token: ''
    };
    expect(reduce({}, { type: types.USER_REGISTER_FAILURE, payload: {} })).toEqual(expectedState);
    expect(global.sessionStorage.setItem.mock.calls.length).toBe(1);
    expect(global.sessionStorage.setItem.mock.calls[0]).toEqual(['jwtToken', '']);
  });
});

describe('User selectors', () => {
  it('should return the logged in state', () => {
    expect(isUserLoggedIn({ user: { isLoggedIn: true } })).toBe(true);
    expect(isUserLoggedIn({ user: { isLoggedIn: false } })).toBe(false);
  });

  it('should return false for an empty state', () => {
    expect(isUserLoggedIn({})).toBe(false);
  });

  it('should return board status', () => {
    const testMessage = 'Failed to validate credentials!';
    expect(getUserStatusText({ user: { error: { status: 401, message: '' } } })).toBe('Failed to login with the email and password provided!');
    expect(getUserStatusText({ user: { error: { status: 409, message: '' } } })).toBe('Email is already registered!');
    expect(getUserStatusText({ user: { error: { status: 400, response: { message: testMessage } } } })).toBe(testMessage);
  });

  it('should return true for logged in state if JWT persists in session storage', () => {
    global.sessionStorage.getItem.mockClear();
    global.sessionStorage.getItem.mockReturnValue('abc-123');
    expect(isUserLoggedIn({})).toBe(true);
  })
});