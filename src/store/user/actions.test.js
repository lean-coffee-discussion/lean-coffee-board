import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import createStore from 'redux';
import { apiMiddleware } from 'redux-api-middleware';
import * as user from './actions';
import * as types from '../actionTypes';
import { Base64 } from 'js-base64';

const baseUrl = process.env.REACT_APP_BASE_API_URL || 'http://localhost:8080/api/v1';

const middleware = [thunk, apiMiddleware];
const createMockStore = configureMockStore(middleware);
const testToken = '123456';
let store;

const mockResponse = (status, statusText, response) => {
  return new window.Response(response, {
    status: status,
    statusText: statusText,
    headers: {
      'Content-type': 'application/json'
    }
  });
};

function successResponse(response) {
  return () => Promise.resolve(mockResponse(200, null, JSON.stringify(response)));
};

function failureResponse(status) {
  return () => Promise.resolve(mockResponse(status, 'A Test Error', JSON.stringify({})));
};

describe('User actions', () => {
  beforeEach(() => {
    store = createMockStore({});
  });

  it('should dispatch USER_LOGIN_SUCCESS when a user logs in', async () => {
    const testUsername = 'TestUser';
    const testPassword = 'TestPassword';
    const encodedAuth = Base64.encode(testUsername + ":" + testPassword);
    const apiResponse = { token: testToken };
    const expectedActions = [
      { type: types.USER_LOGIN_REQUEST },
      { type: types.USER_LOGIN_SUCCESS, payload: apiResponse, meta: undefined }
    ]
    const expectedHeader = { Authorization: `Basic ${encodedAuth}` };
    const mockFetch = jest.fn().mockImplementation(successResponse(apiResponse));
    window.fetch = mockFetch;

    await store.dispatch(user.login(testUsername, testPassword));

    expect(store.getActions()).toEqual(expectedActions);
    expect(mockFetch.mock.calls[0][1].headers).toEqual(expectedHeader);
  });

  it('should dispatch USER_LOGIN_FAILURE when a user logs in incorrectly', async () => {
    const testUsername = 'TestUser';
    const testPassword = 'TestPassword';
    const mockFetch = jest.fn().mockImplementation(failureResponse(401));
    window.fetch = mockFetch;

    await store.dispatch(user.login(testUsername, testPassword));

    expect(store.getActions()[0].type).toEqual(types.USER_LOGIN_REQUEST);
    expect(store.getActions()[1].type).toEqual(types.USER_LOGIN_FAILURE);
  });

  it('should dispatch USER_REGISTER_SUCCESS when a user registers', async () => {
    const testUsername = 'TestUser';
    const testPassword = 'TestPassword';
    const apiResponse = { token: testToken };
    const expectedActions = [
      { type: types.USER_REGISTER_REQUEST },
      { type: types.USER_REGISTER_SUCCESS, payload: apiResponse, meta: undefined }
    ]
    const expectedBody = { email: testUsername, password: testPassword, confirm: testPassword };
    const mockFetch = jest.fn().mockImplementation(successResponse(apiResponse));
    window.fetch = mockFetch;

    await store.dispatch(user.register(testUsername, testPassword, testPassword));

    expect(store.getActions()).toEqual(expectedActions);
    expect(mockFetch.mock.calls[0][1].body).toEqual(JSON.stringify(expectedBody));
  });

  it('should dispatch USER_REGISTER_FAILURE when a user fails to register', async () => {
    const testUsername = 'TestUser';
    const testPassword = 'TestPassword';
    const mockFetch = jest.fn().mockImplementation(failureResponse(401));
    window.fetch = mockFetch;

    await store.dispatch(user.register(testUsername, testPassword, testPassword));

    expect(store.getActions()[0].type).toEqual(types.USER_REGISTER_REQUEST);
    expect(store.getActions()[1].type).toEqual(types.USER_REGISTER_FAILURE);
  });
});