import { CALL_API } from 'redux-api-middleware';
import { Base64 } from 'js-base64';
import * as types from '../actionTypes';
const baseUrl = process.env.REACT_APP_BASE_API_URL || 'http://localhost:8080/api/v1';

export function login(username, password) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/login`,
      method: 'GET',
      headers: { 'Authorization': 'Basic ' + Base64.encode(username + ":" + password) },
      types: [
        types.USER_LOGIN_REQUEST,
        types.USER_LOGIN_SUCCESS,
        types.USER_LOGIN_FAILURE
      ]
    }
  }
};

export function register(username, password, confirmPassword) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/register`,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email: username, password: password, confirm: confirmPassword }),
      types: [
        types.USER_REGISTER_REQUEST,
        types.USER_REGISTER_SUCCESS,
        types.USER_REGISTER_FAILURE
      ]
    }
  }
}