import * as types from '../actionTypes';
import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = {
  isLoggedIn: false
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.USER_LOGIN_REQUEST:
    case types.USER_REGISTER_REQUEST:
      return {
        ...state,
        error: undefined
      };
    case types.USER_LOGIN_SUCCESS:
    case types.USER_REGISTER_SUCCESS:
      if (action.payload.token) {
        sessionStorage.setItem('jwtToken', action.payload.token);
      }
      return {
        ...state,
        token: action.payload.token,
        isLoggedIn: true
      };
    case types.USER_LOGIN_FAILURE:
    case types.USER_REGISTER_FAILURE:
      sessionStorage.setItem('jwtToken', '');
      return {
        ...state,
        error: action.payload,
        token: '',
        isLoggedIn: false
      }
    case LOCATION_CHANGE:
      return {
        ...state,
        error: undefined
      }
    default:
      return state;
  }
}

export function isUserLoggedIn(state) {
  if (sessionStorage.getItem('jwtToken')) {
    return true;
  }
  if (!state.user || !state.user.isLoggedIn) {
    return false;
  }
  return state.user.isLoggedIn;
}

export function getUserStatusText(state) {
  if (!state.user || !state.user.error || !state.user.error.status) {
    return '';
  }
  if (state.user.error.status === 401) {
    return 'Failed to login with the email and password provided!';
  }
  if (state.user.error.status === 409) {
    return 'Email is already registered!';
  }
  if (state.user.error.status === 400 && state.user.error.response && state.user.error.response.message) {
    return state.user.error.response.message;
  }
  return 'Login failed!';
}