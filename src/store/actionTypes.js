//Board Actions
export const CREATE_BOARD = 'boards.CREATE_BOARD';
export const GET_BOARDS_REQUEST = 'boards.GET_BOARDS_REQUEST';
export const GET_BOARDS_SUCCESS = 'boards.GET_BOARDS_SUCCESS';
export const GET_BOARDS_FAILURE = 'boards.GET_BOARDS_FAILURE';
export const POST_BOARD_REQUEST = 'boards.POST_REQUEST';
export const POST_BOARD_SUCCESS = 'boards.POST_SUCCESS';
export const POST_BOARD_FAILURE = 'boards.POST_FAILURE';
export const GET_BOARD_REQUEST = 'boards.GET_BOARD_REQUEST';
export const GET_BOARD_SUCCESS = 'boards.GET_BOARD_SUCCESS';
export const GET_BOARD_FAILURE = 'boards.GET_BOARD_FAILURE';

//Discussion Actions
export const CREATE_DISCUSSION = 'discussions.CREATE_DISCUSSION';
export const MOVE_TOPIC = 'discussions.MOVE_TOPIC';
export const GET_DISCUSSIONS_REQUEST = 'discussions.GET_REQUEST';
export const GET_DISCUSSIONS_SUCCESS = 'discussions.GET_SUCCESS';
export const GET_DISCUSSIONS_FAILURE = 'discussions.GET_FAILURE';
export const POST_DISCUSSION_REQUEST = 'discussions.POST_REQUEST';
export const POST_DISCUSSION_SUCCESS = 'discussions.POST_SUCCESS';
export const POST_DISCUSSION_FAILURE = 'discussions.POST_FAILURE';
export const PUT_DISCUSSION_REQUEST = 'discussions.PUT_REQUEST';
export const PUT_DISCUSSION_SUCCESS = 'discussions.PUT_SUCCESS';
export const PUT_DISCUSSION_FAILURE = 'discussions.PUT_FAILURE';

//Topic Actions
export const CREATE_TOPIC = 'topics.CREATE_TOPIC';
export const GET_TOPICS_REQUEST = 'topics.GET_REQUEST';
export const GET_TOPICS_SUCCESS = 'topics.GET_SUCCESS';
export const GET_TOPICS_FAILURE = 'topics.GET_FAILURE';
export const POST_TOPIC_REQUEST = 'topics.POST_REQUEST';
export const POST_TOPIC_SUCCESS = 'topics.POST_SUCCESS';
export const POST_TOPIC_FAILURE = 'topics.POST_FAILURE';
export const POST_VOTE_REQUEST = 'topics.POST_VOTE_REQUEST';
export const POST_VOTE_SUCCESS = 'topics.POST_VOTE_SUCCESS';
export const POST_VOTE_FAILURE = 'topics.POST_VOTE_FAILURE';

//Timer Actions
export const SET_ELAPSED = 'timers.SET_ELAPSED';
export const START_TIMER = 'timers.START_TIMER';
export const SET_TIME = 'timers.SET_TIME';
export const TOGGLE_TIMER = 'timers.TOGGLE_TIMER';
export const ADD_TIME = 'timers.ADD_TIME';

//User Actions
export const USER_LOGIN_REQUEST = 'user.LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS = 'user.LOGIN_SUCCESS';
export const USER_LOGIN_FAILURE = 'user.LOGIN_FAILURE';
export const USER_REGISTER_REQUEST = 'user.REGISTER_REQUEST';
export const USER_REGISTER_SUCCESS = 'user.REGISTER_SUCCESS';
export const USER_REGISTER_FAILURE = 'user.REGISTER_FAILURE';
export const SET_TOKEN = 'user.SET_TOKEN';