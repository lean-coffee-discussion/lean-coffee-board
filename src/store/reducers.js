import boards from './boards/reducer';
import topics from './topics/reducer';
import timers from './timers/reducer';
import discussions from './discussions/reducer';
import user from './user/reducer';
import { reducer as burgerMenu } from 'redux-burger-menu';

export {
  boards,
  topics,
  timers,
  discussions,
  user,
  burgerMenu
}; 