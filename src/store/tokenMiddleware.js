import { CALL_API } from 'redux-api-middleware';

export default store => next => action => {
  const callApi = action[CALL_API];

  if (callApi) {
    if (!/\/login/.test(callApi.endpoint)) {
      const state = store.getState();
      const sessionToken = sessionStorage.getItem('jwtToken');
      if (state.user && state.user.token) {
        callApi.headers = {
          ...callApi.headers,
          Authorization: `Bearer ${state.user.token}`
        };
      } else if (sessionToken) {
        callApi.headers = {
          ...callApi.headers,
          Authorization: `Bearer ${sessionToken}`
        };
      }
    }
  }

  return next(action);
};