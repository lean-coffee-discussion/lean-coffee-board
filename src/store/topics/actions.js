import * as types from '../actionTypes'
import cuid from 'cuid';
import { getJSON, CALL_API } from 'redux-api-middleware';
const baseUrl = process.env.REACT_APP_BASE_API_URL || 'http://localhost:8080/api/v1';

export function createTopic(discussionId, title, index) {
  return {
    type: types.CREATE_TOPIC,
    payload: {
      topicId: cuid(),
      discussionId: discussionId,
      title: title,
      index: index
    }
  }
}

export function createUserTopic(discussionId, title, index) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/discussions/${discussionId}/topics`,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ title, index }),
      types: [
        types.POST_TOPIC_REQUEST,
        {
          type: types.POST_TOPIC_SUCCESS,
          payload: (action, state, res) => getJSON(res).then((json) => { return { ...json, discussionId } })
        },
        types.POST_TOPIC_FAILURE
      ]
    }
  }
}

export function getTopics(discussionId) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/discussions/${discussionId}/topics`,
      method: 'GET',
      types: [
        types.GET_TOPICS_REQUEST,
        types.GET_TOPICS_SUCCESS,
        types.GET_TOPICS_FAILURE
      ]
    }
  }
}

export function upVoteTopic(topicId) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/topics/${topicId}/votes`,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({}),
      types: [
        types.POST_VOTE_REQUEST,
        types.POST_VOTE_SUCCESS,
        types.POST_VOTE_FAILURE
      ]
    }
  }
}