import * as topics from './actions';
import * as types from '../actionTypes';
import thunk from 'redux-thunk';
import { apiMiddleware } from 'redux-api-middleware';
import configureMockStore from 'redux-mock-store';

const middleware = [thunk, apiMiddleware];
const createMockStore = configureMockStore(middleware);
let store;

const mockResponse = (status, statusText, response) => {
  return new window.Response(response, {
    status: status,
    statusText: statusText,
    headers: {
      'Content-type': 'application/json'
    }
  });
};

function successResponse(response) {
  return () => Promise.resolve(mockResponse(200, null, JSON.stringify(response)));
};

function failureResponse(status) {
  return () => Promise.resolve(mockResponse(status, 'A Test Error', JSON.stringify({})));
};

jest.mock('cuid', () => {
  return jest.fn(() => 'def-456');
});
describe('Topic actions', () => {

  beforeEach(() => {
    store = createMockStore({});
  });

  it('should return an object with type CREATE_TOPIC when a topic is created', () => {
    const title = 'A New Topic Title';
    const index = 99;
    const discussionId = 'abc-123';
    const expectedAction = {
      type: types.CREATE_TOPIC,
      payload: {
        topicId: 'def-456',
        discussionId: discussionId,
        title: title,
        index: index
      }
    }
    expect(topics.createTopic(discussionId, title, index)).toEqual(expectedAction);
  });

  it('should call the correct endpoint with the discussion ID and topic title when a topic is created', async () => {
    const testDiscussionId = '123';
    const testTitle = 'A New Topic Title';
    const testIndex = 99;
    const mockFetch = jest.fn().mockImplementation(successResponse({}));
    window.fetch = mockFetch;

    await store.dispatch(topics.createUserTopic(testDiscussionId, testTitle, testIndex));

    expect(mockFetch.mock.calls.length).toEqual(1);
    expect(mockFetch.mock.calls[0].length).toEqual(2);
    expect(mockFetch.mock.calls[0][0]).toEqual(`http://localhost:8080/api/v1/discussions/${testDiscussionId}/topics`);
    expect(mockFetch.mock.calls[0][1].method).toEqual('POST');
    expect(mockFetch.mock.calls[0][1].body).toEqual(JSON.stringify({ title: testTitle, index: testIndex }));
  });

  it('should dispatch POST_TOPIC_SUCCESS when a topic is created', async () => {
    const testDiscussionId = '123';
    const testTitle = 'A New Topic Title';
    const testIndex = 99;
    const apiResponse = { id: 'abc123', title: testTitle, index: testIndex };
    const expectedActions = [
      { type: types.POST_TOPIC_REQUEST },
      { type: types.POST_TOPIC_SUCCESS, payload: { ...apiResponse, discussionId: testDiscussionId }, meta: undefined }
    ]
    window.fetch = jest.fn().mockImplementation(successResponse(apiResponse));

    await store.dispatch(topics.createUserTopic(testDiscussionId, testTitle, testIndex));

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch POST_TOPIC_FAILURE when a topic cannot be created', async () => {
    const title = 'A New Topic Title';
    window.fetch = jest.fn().mockImplementation(failureResponse(400));

    await store.dispatch(topics.createUserTopic(title));

    expect(store.getActions().length).toEqual(2);
    expect(store.getActions()[1].type).toEqual(types.POST_TOPIC_FAILURE);
  });

  it('should call the correct endpoint with the discussion ID when topics are fetched', async () => {
    const testDiscussionId = '123';
    const mockFetch = jest.fn().mockImplementation(successResponse({}));
    window.fetch = mockFetch;

    await store.dispatch(topics.getTopics(testDiscussionId));

    expect(mockFetch.mock.calls.length).toEqual(1);
    expect(mockFetch.mock.calls[0].length).toEqual(2);
    expect(mockFetch.mock.calls[0][0]).toEqual(`http://localhost:8080/api/v1/discussions/${testDiscussionId}/topics`);
    expect(mockFetch.mock.calls[0][1].method).toEqual('GET');
  });

  it('should dispatch GET_TOPICS_SUCCESS when a topics are fetched', async () => {
    const apiResponse = {
      discussions: [
        { id: 'abc123', title: 'A Test Title', index: 9 },
        { id: 'def456', title: 'Another Test Title', index: 10 }
      ]
    }
    const expectedActions = [
      { type: types.GET_TOPICS_REQUEST },
      { type: types.GET_TOPICS_SUCCESS, payload: apiResponse, meta: undefined }
    ]
    window.fetch = jest.fn().mockImplementation(successResponse(apiResponse));

    await store.dispatch(topics.getTopics());

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch GET_TOPICS_FAILURE when topics cannot be fetched', async () => {
    window.fetch = jest.fn().mockImplementation(failureResponse(400));

    await store.dispatch(topics.getTopics());

    expect(store.getActions().length).toEqual(2);
    expect(store.getActions()[1].type).toEqual(types.GET_TOPICS_FAILURE);
  });

  it('should dispatch POST_VOTE_SUCCESS when a topic is up-voted', async () => {
    const testTopicId = 'abc123';
    const testTitle = 'A Topic Title';
    const testIndex = 99;
    const testVotes = 12;
    const apiResponse = { id: testTopicId, title: testTitle, index: testIndex, votes: testVotes };
    const expectedActions = [
      { type: types.POST_VOTE_REQUEST },
      { type: types.POST_VOTE_SUCCESS, payload: apiResponse, meta: undefined }
    ]
    window.fetch = jest.fn().mockImplementation(successResponse(apiResponse));

    await store.dispatch(topics.upVoteTopic(testTopicId));

    expect(store.getActions()).toEqual(expectedActions);
  });
});