import * as types from '../actionTypes';
import _ from 'lodash';

const initialState = {
  byId: {},
  maxIndex: 0
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.CREATE_TOPIC: {
      const { payload } = action;
      const { topicId, title, index, votes } = payload;
      const currentMaxIndex = state.maxIndex || 0;
      const newTopicIndex = index || 0;
      const newTopicVotes = votes || 0;
      return {
        ...state,
        byId: {
          ...state.byId,
          [topicId]: {
            id: topicId,
            title: title,
            index: newTopicIndex,
            votes: newTopicVotes
          }
        },
        maxIndex: Math.max(currentMaxIndex, newTopicIndex)
      };
    }
    case types.POST_TOPIC_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const newTopicIndex = action.payload.index || 0;
      const newTopicVotes = action.payload.votes || 0;
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.id]: {
            id: action.payload.id,
            title: action.payload.title,
            index: newTopicIndex,
            votes: newTopicVotes
          }
        },
        maxIndex: Math.max(currentMaxIndex, newTopicIndex)
      }
    }
    case types.GET_TOPICS_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const topicsById = _.keyBy(action.payload.topics, (topic) => topic.id);
      const maxTopicIndex = _.reduce(action.payload.topics, (maxIndex, topic) => {
        return Math.max(maxIndex, topic.index);
      }, currentMaxIndex);
      return {
        ...state,
        byId: topicsById,
        maxIndex: maxTopicIndex
      }
    }
    case types.GET_BOARD_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const topicsById = _.keyBy(action.payload.entities.topics, (topic) => topic.id);
      const maxTopicIndex = _.reduce(action.payload.entities.topics, (maxIndex, topic) => {
        return Math.max(maxIndex, topic.index);
      }, currentMaxIndex);
      return {
        ...state,
        byId: topicsById,
        maxIndex: maxTopicIndex
      }
    }
    case types.POST_VOTE_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const newTopicIndex = action.payload.index || 0;
      const topicVotes = action.payload.votes || 0;
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.id]: {
            id: action.payload.id,
            title: action.payload.title,
            index: newTopicIndex,
            votes: topicVotes
          }
        },
        maxIndex: Math.max(currentMaxIndex, newTopicIndex)
      }
    }
    default:
      return state;
  }
}

export function getTopicsById(state) {
  if (!state.topics || !state.topics.byId) {
    return {};
  }
  return state.topics.byId;
}

export function getTopicsIdArray(state) {
  if (!state.topics || !state.topics.byId) {
    return [];
  }
  return _.reduce(_.sortBy(state.topics.byId, ['index']), (topicArray, topic) => {
    topicArray.push(topic.id)
    return topicArray
  }, []);
}

export function getNextTopicIndex(state) {
  if (!state.topics || !Number.isInteger(state.topics.maxIndex)) {
    return 0;
  }
  return state.topics.maxIndex + 1;
}