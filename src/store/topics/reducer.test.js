import reduce, { getTopicsById, getTopicsIdArray, getNextTopicIndex } from './reducer';
import * as types from '../actionTypes';

describe('topics reducer', () => {
  const existingState = {
    byId: {
      'abc-123': {
        id: 'abc-123', title: 'An Existing Topic', index: 98
      }
    },
    maxIndex: 98
  };

  it('should return the initial state', () => {
    expect(reduce(undefined, {})).toEqual({ byId: {}, maxIndex: 0 })
  });

  it('should handle CREATE_TOPIC with a new state', () => {
    const newTopicTitle = 'A New Topic Title';
    const newTopicIndex = 99;
    const expectedState = {
      byId: {
        'def-456': {
          id: 'def-456',
          title: newTopicTitle,
          index: newTopicIndex,
          votes: 0
        }
      },
      maxIndex: newTopicIndex
    };
    expect(reduce([], {
      type: types.CREATE_TOPIC,
      payload: {
        topicId: 'def-456',
        discussionId: '456-789',
        title: newTopicTitle,
        index: newTopicIndex
      }
    })
    ).toEqual(expectedState);
  });

  it('should handle CREATE_TOPIC with an existing state', () => {
    const newTopicTitle = 'A New Topic Title';
    const newTopicIndex = 99;
    const expectedState = {
      byId: {
        'abc-123': {
          id: 'abc-123',
          title: 'An Existing Topic',
          index: 98
        },
        'def-456': {
          id: 'def-456',
          title: newTopicTitle,
          index: newTopicIndex,
          votes: 0
        }
      },
      maxIndex: newTopicIndex
    };
    expect(reduce(
      existingState,
      {
        type: types.CREATE_TOPIC,
        payload: {
          topicId: 'def-456',
          discussionId: '456-789',
          title: newTopicTitle,
          index: newTopicIndex
        }
      }
    )
    ).toEqual(expectedState);
  });

  it('should set a list of topics into the state', () => {
    const maxTopicIndex = 7;
    const apiResponse = {
      topics:
        [{
          id: 'abc-123',
          title: 'An Existing Topic',
          index: 6
        },
        {
          id: 'def-456',
          title: 'New Topic Title',
          index: maxTopicIndex
        }]
    };
    const expectedState = {
      byId: {
        'abc-123': {
          id: 'abc-123',
          title: 'An Existing Topic',
          index: 6
        },
        'def-456': {
          id: 'def-456',
          title: 'New Topic Title',
          index: maxTopicIndex
        }
      },
      maxIndex: maxTopicIndex
    };
    expect(reduce({}, { type: types.GET_TOPICS_SUCCESS, payload: apiResponse })).toEqual(expectedState);
  });

  it('should handle POST_TOPIC_SUCCESS', () => {
    const newTopicTitle = 'A New Topic Title';
    const newTopicId = 'def-456';
    const newTopicIndex = 99;
    const testVotes = 12;
    const expectedState = {
      byId: {
        [newTopicId]: {
          id: newTopicId,
          title: newTopicTitle,
          index: newTopicIndex,
          votes: testVotes
        }
      },
      maxIndex: newTopicIndex
    };
    expect(reduce({}, {
      type: types.POST_TOPIC_SUCCESS,
      payload: {
        id: newTopicId, title: newTopicTitle, index: newTopicIndex, votes: testVotes
      }
    })
    ).toEqual(expectedState);
  });

  it('should set a board topics into the state', () => {
    const testTopicTitle = 'An Existing Topic';
    const testTopicId = 'ghi-789';
    const testTopicIndex = 99;
    const apiResponse = {
      result: 'abc-123',
      entities: {
        topics: {
          [testTopicId]: {
            id: testTopicId,
            title: testTopicTitle,
            index: testTopicIndex
          }
        }
      }
    };
    const expectedState = {
      byId: {
        [testTopicId]: {
          id: testTopicId,
          title: testTopicTitle,
          index: testTopicIndex
        }
      },
      maxIndex: testTopicIndex
    };
    expect(reduce({}, { type: types.GET_BOARD_SUCCESS, payload: apiResponse })).toEqual(expectedState);
  });

  it('should handle POST_VOTE_SUCCESS', () => {
    const votedTopicTitle = 'Topic that has been upvoted';
    const testTopicId = 'def-456';
    const testTopicIndex = 99;
    const testVotes = 12;
    const expectedState = {
      byId: {
        [testTopicId]: {
          id: testTopicId,
          title: votedTopicTitle,
          index: testTopicIndex,
          votes: testVotes
        }
      },
      maxIndex: testTopicIndex
    };
    expect(reduce({
      byId: {
        [testTopicId]: {
          id: testTopicId,
          title: votedTopicTitle,
          index: testTopicIndex,
          votes: 0
        }
      },
      maxIndex: testTopicIndex
    }, {
        type: types.POST_VOTE_SUCCESS,
        payload: {
          id: testTopicId, title: votedTopicTitle, index: testTopicIndex, votes: testVotes
        }
      })
    ).toEqual(expectedState);
  });
});

describe('topics selectors', () => {
  const testState = {
    topics: {
      byId: {
        'abc-123': {
          id: 'abc-123',
          title: 'An Existing Topic',
          index: 6
        },
        'def-456': {
          id: 'def-456',
          title: 'New Topic Title',
          index: 5
        }
      },
      maxIndex: 6
    }
  };

  it('should get topics by their id', () => {
    expect(getTopicsById(testState)).toEqual(testState.topics.byId);
  });

  it('should get an array of topic ids', () => {
    expect(getTopicsIdArray(testState)).toEqual(['def-456', 'abc-123']);
  });

  it('should return an empty state', () => {
    expect(getTopicsById({})).toEqual({});
  });

  it('should return an empty state array', () => {
    expect(getTopicsIdArray({})).toEqual([]);
  });

  it('should return the new max index for the topics', () => {
    expect(getNextTopicIndex({ topics: { maxIndex: 98 } })).toEqual(99);
  });
});