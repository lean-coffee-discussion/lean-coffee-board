import * as types from '../actionTypes';
import cuid from 'cuid';
import { CALL_API } from 'redux-api-middleware';
const baseUrl = process.env.REACT_APP_BASE_API_URL || 'http://localhost:8080/api/v1';

export function createBoard(title, index) {
  return {
    type: types.CREATE_BOARD,
    payload: {
      boardId: cuid(), title, index
    }
  }
}

export function createUserBoard(title, index) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/boards`,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ title: title, index: index }),
      types: [
        types.POST_BOARD_REQUEST,
        types.POST_BOARD_SUCCESS,
        types.POST_BOARD_FAILURE
      ]
    }
  }
}

export function getBoards() {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/boards`,
      method: 'GET',
      types: [
        types.GET_BOARDS_REQUEST,
        types.GET_BOARDS_SUCCESS,
        types.GET_BOARDS_FAILURE
      ]
    }
  }
}

export function getBoard(boardId) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/boards/${boardId}`,
      method: 'GET',
      types: [
        types.GET_BOARD_REQUEST,
        types.GET_BOARD_SUCCESS,
        types.GET_BOARD_FAILURE
      ]
    }
  }
}