import * as boards from './actions';
import * as types from '../actionTypes';
import thunk from 'redux-thunk';
import { apiMiddleware } from 'redux-api-middleware';
import configureMockStore from 'redux-mock-store';

const middleware = [thunk, apiMiddleware];
const createMockStore = configureMockStore(middleware);
var store;

const mockResponse = (status, statusText, response) => {
  return new window.Response(response, {
    status: status,
    statusText: statusText,
    headers: {
      'Content-type': 'application/json'
    }
  });
};

function successResponse(response) {
  return () => Promise.resolve(mockResponse(200, null, JSON.stringify(response)));
};

function failureResponse(status) {
  return () => Promise.resolve(mockResponse(status, 'A Test Error', JSON.stringify({})));
};

jest.mock('cuid', () => {
  return jest.fn(() => 'def-456');
});

describe('Board actions', () => {
  beforeEach(() => {
    store = createMockStore({});
  });

  it('should return an object with type CREATE_BOARD when a board is created', () => {
    const title = 'A New Board Title';
    const index = 99;
    const expectedAction = {
      type: types.CREATE_BOARD,
      payload: {
        boardId: 'def-456',
        title,
        index
      }
    }
    expect(boards.createBoard(title, index)).toEqual(expectedAction);
  });

  it('should call the correct endpoint with the board title when a board is created', async () => {
    const testTitle = 'A New Board Title';
    const testIndex = 99;
    const mockFetch = jest.fn().mockImplementation(successResponse({}));
    window.fetch = mockFetch;

    await store.dispatch(boards.createUserBoard(testTitle, testIndex));

    expect(mockFetch.mock.calls.length).toEqual(1);
    expect(mockFetch.mock.calls[0].length).toEqual(2);
    expect(mockFetch.mock.calls[0][0]).toEqual('http://localhost:8080/api/v1/boards');
    expect(mockFetch.mock.calls[0][1].method).toEqual('POST');
    expect(mockFetch.mock.calls[0][1].body).toEqual(JSON.stringify({ title: testTitle, index: testIndex }));
  });

  it('should dispatch POST_BOARD_SUCCESS when a board is created', async () => {
    const title = 'A New Board Title';
    const index = 99;
    const apiResponse = { id: 'abc123', title: title, index: index };
    const expectedActions = [
      { type: types.POST_BOARD_REQUEST },
      { type: types.POST_BOARD_SUCCESS, payload: apiResponse, meta: undefined }
    ]
    const expectedBody = { title, index };
    const mockFetch = jest.fn().mockImplementation(successResponse(apiResponse));
    window.fetch = mockFetch;

    await store.dispatch(boards.createUserBoard(title, index));

    expect(store.getActions()).toEqual(expectedActions);
    expect(mockFetch.mock.calls[0][1].body).toBe(JSON.stringify(expectedBody));
  });

  it('should dispatch POST_BOARD_FAILURE when a board cannot be created', async () => {
    const title = 'A New Board Title';
    window.fetch = jest.fn().mockImplementation(failureResponse(400));

    await store.dispatch(boards.createUserBoard(title, 0));

    expect(store.getActions().length).toEqual(2);
    expect(store.getActions()[1].type).toEqual(types.POST_BOARD_FAILURE);
  });

  it('should call the correct endpoint when the boards are fetched', async () => {
    const mockFetch = jest.fn().mockImplementation(successResponse({}));
    window.fetch = mockFetch;

    await store.dispatch(boards.getBoards());

    expect(mockFetch.mock.calls.length).toEqual(1);
    expect(mockFetch.mock.calls[0].length).toEqual(2);
    expect(mockFetch.mock.calls[0][0]).toEqual('http://localhost:8080/api/v1/boards');
    expect(mockFetch.mock.calls[0][1].method).toEqual('GET');
  });

  it('should dispatch GET_BOARDS_SUCCESS when boards are fetched', async () => {
    const apiResponse = {
      boards: [
        { id: 'abc123', title: 'A Test Title', index: 0 },
        { id: 'def456', title: 'Another Test Title', index: 1 }
      ]
    };
    const expectedActions = [
      { type: types.GET_BOARDS_REQUEST },
      { type: types.GET_BOARDS_SUCCESS, payload: apiResponse, meta: undefined }
    ]
    window.fetch = jest.fn().mockImplementation(successResponse(apiResponse));

    await store.dispatch(boards.getBoards());

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch GET_BOARDS_FAILURE when boards cannot be fetched', async () => {
    window.fetch = jest.fn().mockImplementation(failureResponse(400));

    await store.dispatch(boards.getBoards());

    expect(store.getActions().length).toEqual(2);
    expect(store.getActions()[1].type).toEqual(types.GET_BOARDS_FAILURE);
  });

  it('should dispatch GET_BOARD_SUCCESS when a board is fetched', async () => {
    const testBoardId = 'abc-123';
    const apiResponse = {
      boards: {
        [testBoardId]: { id: testBoardId, title: 'A Test Title', index: 0 }
      }
    };
    const expectedActions = [
      { type: types.GET_BOARD_REQUEST },
      { type: types.GET_BOARD_SUCCESS, payload: apiResponse, meta: undefined }
    ]
    window.fetch = jest.fn().mockImplementation(successResponse(apiResponse));

    await store.dispatch(boards.getBoard(testBoardId));

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch GET_BOARD_FAILURE when a board cannot be fetched', async () => {
    window.fetch = jest.fn().mockImplementation(failureResponse(400));

    await store.dispatch(boards.getBoard('abc-123'));

    expect(store.getActions().length).toEqual(2);
    expect(store.getActions()[1].type).toEqual(types.GET_BOARD_FAILURE);
  });
});