import reduce, { getBoardsById, getBoardIdArray, getBoardStatusText, getNextBoardIndex, getBoardDiscussions } from './reducer';
import * as types from '../actionTypes';

describe('boards reducer', () => {
  const existingState = {
    byId: {
      'abc-123': {
        id: 'abc-123', title: 'An Existing Board', index: 4
      }
    },
    maxIndex: 4,
    isLoading: false
  };

  it('should return the initial state', () => {
    expect(reduce(undefined, {})).toEqual({ byId: {}, isLoading: false, error: undefined, maxIndex: 0 })
  });

  it('should handle CREATE_BOARD with a new state', () => {
    const newBoardTitle = 'A New Board Title';
    const newBoardIndex = 99;
    const expectedState = {
      byId: {
        'def-456': {
          id: 'def-456',
          title: newBoardTitle,
          index: newBoardIndex
        }
      },
      maxIndex: newBoardIndex
    };
    expect(reduce({}, {
      type: types.CREATE_BOARD,
      payload: {
        boardId: 'def-456',
        title: newBoardTitle,
        index: newBoardIndex
      }
    })
    ).toEqual(expectedState);
  });

  it('should handle CREATE_BOARD with an existing state', () => {
    const newBoardTitle = 'A New Board Title';
    const newMaxIndex = 8;
    const expectedState = {
      byId: {
        'abc-123': {
          id: 'abc-123',
          title: 'An Existing Board',
          index: 4
        },
        'def-456': {
          id: 'def-456',
          title: newBoardTitle,
          index: newMaxIndex
        }
      },
      isLoading: false,
      maxIndex: newMaxIndex
    };
    expect(reduce(
      existingState,
      {
        type: types.CREATE_BOARD,
        payload: {
          boardId: 'def-456',
          title: newBoardTitle,
          index: newMaxIndex
        }
      }
    )
    ).toEqual(expectedState);
  });

  it('should add discussions to the board when a discussion is added', () => {
    const newDiscussionId = 'abc-123';
    const boardId = 'def-456';
    const existingState = {
      byId: {
        [boardId]: {
          id: boardId,
          title: 'An Existing Board'
        }
      }
    }
    var expectedState = {
      byId: {
        [boardId]: {
          id: boardId,
          title: 'An Existing Board',
          discussions: [newDiscussionId]
        }
      }
    };

    const result = reduce(existingState, {
      type: types.CREATE_DISCUSSION,
      payload: {
        discussionId: newDiscussionId,
        boardId: boardId,
        title: 'A New Discussion'
      }
    });
    expect(result).toEqual(expectedState);
  });

  it('should set a list of boards into the state', () => {
    const testMaxIndex = 8;
    const apiResponse = {
      boards:
        [{
          id: 'abc-123',
          title: 'An Existing Board',
          index: 4
        },
        {
          id: 'def-456',
          title: 'New Board Title',
          index: testMaxIndex
        }]
    };
    const expectedState = {
      byId: {
        'abc-123': {
          id: 'abc-123',
          title: 'An Existing Board',
          index: 4
        },
        'def-456': {
          id: 'def-456',
          title: 'New Board Title',
          index: testMaxIndex
        }
      },
      maxIndex: testMaxIndex
    };
    expect(reduce({}, { type: types.GET_BOARDS_SUCCESS, payload: apiResponse })).toEqual(expectedState);
  });

  it('should handle POST_BOARD_SUCCESS', () => {
    const newBoardTitle = 'A New Board Title';
    const newBoardId = 'def-456';
    const newBoardIndex = 99;
    const expectedState = {
      byId: {
        [newBoardId]: {
          id: newBoardId,
          title: newBoardTitle,
          index: newBoardIndex
        },
      },
      maxIndex: 99
    };
    expect(reduce({}, {
      type: types.POST_BOARD_SUCCESS,
      payload: { id: newBoardId, title: newBoardTitle, index: newBoardIndex }
    })
    ).toEqual(expectedState);
  });

  it('should add discussions to the board when a discussion is posted to the API', () => {
    const newDiscussionId = 'abc-123';
    const boardId = 'def-456';
    const existingState = {
      byId: {
        [boardId]: {
          id: boardId,
          title: 'An Existing Board'
        }
      }
    }
    var expectedState = {
      byId: {
        [boardId]: {
          id: boardId,
          title: 'An Existing Board',
          discussions: [newDiscussionId]
        }
      }
    };

    const result = reduce(existingState, {
      type: types.POST_DISCUSSION_SUCCESS,
      payload: {
        id: newDiscussionId,
        boardId: boardId,
        title: 'A New Discussion'
      }
    });
    expect(result).toEqual(expectedState);
  });

  it('should set a board into the state', () => {
    const testBoardId = 'abc-123';
    const testBoardTitle = 'An Existing Board';
    const testBoardIndex = 99;
    const apiResponse = {
      result: testBoardId,
      entities: {
        boards: {
          [testBoardId]: {
            id: testBoardId,
            title: testBoardTitle,
            index: testBoardIndex
          }
        }
      }
    };
    const expectedState = {
      byId: {
        [testBoardId]: {
          id: testBoardId,
          title: testBoardTitle,
          index: testBoardIndex
        }
      },
      isLoading: false,
      maxIndex: testBoardIndex
    };
    expect(reduce({ isLoading: true }, { type: types.GET_BOARD_SUCCESS, payload: apiResponse })).toEqual(expectedState);
  });

  it('should set a board into an existing state without removing existing boards', () => {
    const testBoardId = 'def-456';
    const testBoardTitle = 'A New Board';
    const testBoardIndex = 99;
    const apiResponse = {
      result: testBoardId,
      entities: {
        boards: {
          [testBoardId]: {
            id: testBoardId,
            title: testBoardTitle,
            index: testBoardIndex
          }
        }
      }
    };
    const expectedState = {
      byId: {
        'abc-123': {
          id: 'abc-123', title: 'An Existing Board', index: 4
        },
        [testBoardId]: {
          id: testBoardId,
          title: testBoardTitle,
          index: testBoardIndex
        }
      },
      maxIndex: 99,
      isLoading: false
    };
    expect(reduce(existingState, { type: types.GET_BOARD_SUCCESS, payload: apiResponse })).toEqual(expectedState);
  });

  it('should set a loading state when a board is requested', () => {
    const expectedState = { isLoading: true };
    expect(reduce({}, { type: types.GET_BOARD_REQUEST })).toEqual(expectedState);
  });

  it('should handle an error if the board is not found', () => {
    const expectedState = {
      error: {
        name: 'ApiError',
        status: 404,
        statusText: 'Not Found',
        message: '404 - Not Found'
      },
      isLoading: false
    };
    expect(reduce({}, {
      type: types.GET_BOARD_FAILURE,
      payload: {
        name: 'ApiError',
        status: 404,
        statusText: 'Not Found',
        message: '404 - Not Found'
      }
    })).toEqual(expectedState);
  });

  it('should update the maxIndex when a board is posted', () => {
    const newBoardTitle = 'A New Board Title';
    const newBoardId = 'def-456';
    const newBoardIndex = 99;
    const expectedState = {
      byId: {
        [newBoardId]: {
          id: newBoardId,
          title: newBoardTitle,
          index: newBoardIndex
        }
      },
      maxIndex: newBoardIndex
    };
    expect(reduce({}, {
      type: types.POST_BOARD_SUCCESS,
      payload: { id: newBoardId, title: newBoardTitle, index: newBoardIndex }
    })
    ).toEqual(expectedState);
  });
});

describe('boards selectors', () => {
  const firstBoardId = 'def-456';
  const secondBoardId = 'abc-123';
  const firstDiscussionArray = ['abc', 'ghi'];
  const testState = {
    boards: {
      byId: {
        [secondBoardId]: {
          id: secondBoardId,
          title: 'An Existing Board',
          index: 6,
          discussions: ['def', 'jkl']
        },
        [firstBoardId]: {
          id: firstBoardId,
          title: 'New Board Title',
          index: 5,
          discussions: firstDiscussionArray
        }
      }
    }
  };
  const testProps = {
    match: { params: { boardId: firstBoardId } }
  };

  it('should get boards by their id', () => {
    expect(getBoardsById(testState)).toEqual(testState.boards.byId);
  });

  it('should get an array of board ids', () => {
    expect(getBoardIdArray(testState)).toEqual(['def-456', 'abc-123']);
  });

  it('should return an empty state', () => {
    expect(getBoardsById({})).toEqual({});
  });

  it('should return an empty state array', () => {
    expect(getBoardIdArray({})).toEqual([]);
  });

  it('should return board status', () => {
    expect(getBoardStatusText({ boards: { isLoading: true } })).toBe('Loading Board...');
    expect(getBoardStatusText({ boards: { isLoading: false, error: { status: 404 } } })).toBe('Board Not Found!');
  });

  it('should keep track of the maximum board index', () => {
    expect(getNextBoardIndex({ boards: { maxIndex: 55 } })).toBe(56);
  });

  it('should return a list of discussions associated with a particular board', () => {
    expect(getBoardDiscussions(testState, testProps)).toBe(firstDiscussionArray);
  });
});