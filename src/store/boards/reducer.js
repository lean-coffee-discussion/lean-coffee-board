import * as types from '../actionTypes';
import _ from 'lodash';

const initialState = {
  byId: {},
  isLoading: false,
  error: undefined,
  maxIndex: 0
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.CREATE_BOARD: {
      const currentMaxIndex = state.maxIndex || 0;
      const newBoardIndex = action.payload.index || 0;
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.boardId]: {
            id: action.payload.boardId,
            title: action.payload.title,
            index: newBoardIndex
          }
        },
        maxIndex: Math.max(currentMaxIndex, newBoardIndex)
      };
    }
    case types.CREATE_DISCUSSION: {
      const board = state.byId[action.payload.boardId];
      if (!board.discussions) { board.discussions = [] };
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.boardId]: {
            ...board,
            discussions: _.concat(board.discussions, action.payload.discussionId)
          }
        }
      };
    }
    case types.POST_BOARD_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const newBoardIndex = action.payload.index || 0;
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.id]: {
            id: action.payload.id,
            title: action.payload.title,
            index: newBoardIndex
          }
        },
        maxIndex: Math.max(currentMaxIndex, newBoardIndex)
      }
    }

    case types.GET_BOARDS_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const boardsById = _.keyBy(action.payload.boards, (board) => board.id);
      const maxBoardIndex = _.reduce(action.payload.boards, (maxIndex, board) => {
        return Math.max(maxIndex, board.index);
      }, currentMaxIndex);
      return {
        ...state,
        byId: boardsById,
        maxIndex: maxBoardIndex
      }
    }
    case types.POST_DISCUSSION_SUCCESS: {
      const board = state.byId[action.payload.boardId];
      if (!board.discussions) { board.discussions = [] };
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.boardId]: {
            ...board,
            discussions: _.concat(board.discussions, action.payload.id)
          }
        }
      };
    }
    case types.GET_BOARD_SUCCESS: {
      const boardsById = action.payload.entities.boards;
      const currentMaxIndex = state.maxIndex || 0;
      const maxBoardIndex = _.reduce(action.payload.entities.boards, (maxIndex, board) => {
        return Math.max(maxIndex, board.index);
      }, currentMaxIndex);
      return {
        ...state,
        byId: {
          ...state.byId,
          ...boardsById
        },
        isLoading: false,
        maxIndex: maxBoardIndex
      }
    }
    case types.GET_BOARD_REQUEST: {
      return {
        ...state,
        isLoading: true,
        error: undefined
      }
    }
    case types.GET_BOARD_FAILURE: {
      return {
        ...state,
        error: action.payload,
        isLoading: false
      }
    }
    default:
      return state;
  }
}

export function getBoardsById(state) {
  if (!state.boards || !state.boards.byId) {
    return {};
  }
  return state.boards.byId;
}

export function getBoardIdArray(state) {
  if (!state.boards || !state.boards.byId) {
    return [];
  }
  return _.reduce(_.sortBy(state.boards.byId, ['index']), (boardArray, board) => {
    boardArray.push(board.id)
    return boardArray
  }, []);
}

export function getBoardStatusText(state) {
  if (!state.boards) {
    return 'Error Loading Application State!';
  }
  if (state.boards.isLoading) {
    return 'Loading Board...';
  }
  if (state.boards.error) {
    if (state.boards.error.status === 404) {
      return 'Board Not Found!';
    } else {
      return 'Error Loading Board!';
    }
  }
}

export function getNextBoardIndex(state) {
  if (!state.boards || !Number.isInteger(state.boards.maxIndex)) {
    return 0;
  }
  return state.boards.maxIndex + 1;
}

export function getBoardDiscussions(state, props) {
  const boardId = _.get(props, 'match.params.boardId');
  if (!boardId
    || !state.boards
    || !state.boards.byId
    || !state.boards.byId[boardId]
    || !state.boards.byId[boardId].discussions) {
    return [];
  }
  return state.boards.byId[boardId].discussions;
}