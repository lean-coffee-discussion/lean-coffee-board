import * as types from '../actionTypes';
import _ from 'lodash';

const initialState = {
  byId: {},
  maxIndex: 0
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.CREATE_DISCUSSION: {
      const currentMaxIndex = state.maxIndex || 0;
      const newDiscussionIndex = action.payload.index || 0;
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.discussionId]: {
            id: action.payload.discussionId,
            title: action.payload.title,
            index: newDiscussionIndex,
            topics: []
          }
        },
        maxIndex: Math.max(currentMaxIndex, newDiscussionIndex)
      };
    }
    case types.CREATE_TOPIC: {
      const discussion = state.byId[action.payload.discussionId];
      if (!discussion.topics) { discussion.topics = [] };
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.discussionId]: {
            ...discussion,
            topics: _.concat(discussion.topics, action.payload.topicId)
          }
        }
      };
    }
    case types.MOVE_TOPIC:
      const sourceDiscussion = state.byId[action.payload.sourceDiscussionId];
      const destDiscussion = state.byId[action.payload.destDiscussionId];
      if (!destDiscussion.topics) { destDiscussion.topics = [] };
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.sourceDiscussionId]: {
            ...sourceDiscussion,
            topics: _.pull(sourceDiscussion.topics, action.payload.topicId)
          },
          [action.payload.destDiscussionId]: {
            ...destDiscussion,
            topics: _.concat(destDiscussion.topics, action.payload.topicId)
          }
        }
      }
    case types.POST_DISCUSSION_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const newDiscussionIndex = action.payload.index || 0;
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.id]: {
            id: action.payload.id,
            title: action.payload.title,
            index: newDiscussionIndex
          }
        },
        maxIndex: Math.max(currentMaxIndex, newDiscussionIndex)
      };
    }
    case types.POST_TOPIC_SUCCESS: {
      const discussion = state.byId[action.payload.discussionId];
      if (!discussion.topics) { discussion.topics = [] };
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.discussionId]: {
            ...discussion,
            topics: _.concat(discussion.topics, action.payload.id)
          }
        }
      };
    }
    case types.GET_DISCUSSIONS_SUCCESS:
      const currentMaxIndex = state.maxIndex || 0;
      const discussionsById = _.keyBy(action.payload.discussions, (discussion) => discussion.id);
      const maxDiscussionIndex = _.reduce(action.payload.discussions, (maxIndex, discussion) => {
        return Math.max(maxIndex, discussion.index);
      }, currentMaxIndex);
      return {
        ...state,
        byId: discussionsById,
        maxIndex: maxDiscussionIndex
      }
    case types.GET_BOARD_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const discussionsById = _.keyBy(action.payload.entities.discussions, (discussion) => discussion.id);
      const maxDiscussionIndex = _.reduce(action.payload.entities.discussions, (maxIndex, discussion) => {
        return Math.max(maxIndex, discussion.index);
      }, currentMaxIndex);
      return {
        ...state,
        byId: discussionsById,
        maxIndex: maxDiscussionIndex
      }
    }
    case types.PUT_DISCUSSION_SUCCESS: {
      const currentMaxIndex = state.maxIndex || 0;
      const updatedDiscussionIndex = action.payload.index || 0;
      const topicArray = action.payload.topics || [];
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.id]: {
            id: action.payload.id,
            title: action.payload.title,
            index: updatedDiscussionIndex,
            topics: topicArray
          }
        },
        maxIndex: Math.max(currentMaxIndex, updatedDiscussionIndex)
      }
    }
    default:
      return state;
  }
}

export function getDiscussionsById(state) {
  if (!state.discussions || !state.discussions.byId) {
    return {};
  }
  return state.discussions.byId;
}

export function getDiscussionsIdArray(state) {
  if (!state.discussions || !state.discussions.byId) {
    return [];
  }
  return _.reduce(_.sortBy(state.discussions.byId, ['index']), (discussionArray, discussion) => {
    discussionArray.push(discussion.id)
    return discussionArray
  }, []);
}

export function getNextDiscussionIndex(state) {
  if (!state.discussions || !Number.isInteger(state.discussions.maxIndex)) {
    return 0;
  }
  return state.discussions.maxIndex + 1;
}

export function getDiscussionTopics(state, props) {
  const discussionId = props.discussionId;
  if (!discussionId
    || !state.discussions
    || !state.discussions.byId
    || !state.discussions.byId[discussionId]
    || !state.discussions.byId[discussionId].topics) {
    return [];
  }
  return state.discussions.byId[discussionId].topics;
}