import reduce, { getDiscussionsById, getDiscussionsIdArray, getNextDiscussionIndex, getDiscussionTopics } from './reducer';
import * as types from '../actionTypes';

jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => 'def-456')
  };
});

describe('discussions reducer', () => {

  it('should return the initial state', () => {
    expect(reduce(undefined, {})).toEqual({ byId: {}, maxIndex: 0 });
  });

  it('should allow new discussions to be added', () => {
    const newDiscussionTitle = 'To Discuss';
    const expectedState = {
      byId: {
        'def-456': {
          id: 'def-456',
          title: newDiscussionTitle,
          index: 99,
          topics: []
        }
      },
      maxIndex: 99
    }
    expect(reduce(undefined, {
      type: types.CREATE_DISCUSSION,
      payload: {
        discussionId: 'def-456',
        title: newDiscussionTitle,
        index: 99
      }
    })).toEqual(expectedState);
  });

  it('should add topics to the discussion when a topic is added', () => {
    const newTopicId = 'abc-123';
    const discussionId = 'def-456';
    const existingState = {
      byId: {
        [discussionId]: {
          id: discussionId,
          title: 'An Existing Discussion'
        }
      }
    }
    var expectedState = {
      byId: {
        [discussionId]: {
          id: discussionId,
          title: 'An Existing Discussion',
          topics: [newTopicId]
        }
      }
    };

    const result = reduce(existingState, {
      type: types.CREATE_TOPIC,
      payload: {
        topicId: newTopicId,
        discussionId: discussionId,
        title: 'A New Topic'
      }
    });
    expect(result).toEqual(expectedState);
  });

  it('should move a topic when MOVE_TOPIC is called', () => {
    const firstDiscussionId = 'def-456';
    const secondDiscussionId = 'ghi-789';
    const topicId = '123456';
    const existingState = {
      byId: {
        [firstDiscussionId]: {
          id: firstDiscussionId,
          title: 'The First Discussion',
          topics: [topicId, '789012']
        },
        [secondDiscussionId]: {
          id: secondDiscussionId,
          title: 'The Second Discussion',
          topics: []
        }
      }
    }
    const expectedState = {
      byId: {
        [firstDiscussionId]: {
          id: firstDiscussionId,
          title: 'The First Discussion',
          topics: ['789012']
        },
        [secondDiscussionId]: {
          id: secondDiscussionId,
          title: 'The Second Discussion',
          topics: [topicId]
        }
      }
    }
    const result = reduce(existingState, {
      type: types.MOVE_TOPIC,
      payload: {
        topicId: topicId,
        sourceDiscussionId: firstDiscussionId,
        destDiscussionId: secondDiscussionId
      }
    });
    expect(result).toEqual(expectedState);
  });

  it('should set a list of discussions into the state', () => {
    const apiResponse = {
      discussions:
        [{
          id: 'abc-123',
          title: 'An Existing Discussion',
          index: 98
        },
        {
          id: 'def-456',
          title: 'New Discussion Title',
          index: 99
        }]
    };
    const expectedState = {
      byId: {
        'abc-123': {
          id: 'abc-123',
          title: 'An Existing Discussion',
          index: 98
        },
        'def-456': {
          id: 'def-456',
          title: 'New Discussion Title',
          index: 99
        }
      },
      maxIndex: 99
    };
    expect(reduce({}, { type: types.GET_DISCUSSIONS_SUCCESS, payload: apiResponse })).toEqual(expectedState);
  });

  it('should handle POST_DISCUSSION_SUCCESS', () => {
    const newDiscussionTitle = 'A New Discussion Title';
    const newDiscussionId = 'def-456';
    const newDiscussionIndex = 99;
    const expectedState = {
      byId: {
        [newDiscussionId]: {
          id: newDiscussionId,
          title: newDiscussionTitle,
          index: newDiscussionIndex
        }
      },
      maxIndex: newDiscussionIndex
    };
    expect(reduce({}, {
      type: types.POST_DISCUSSION_SUCCESS,
      payload: { id: newDiscussionId, title: newDiscussionTitle, index: newDiscussionIndex }
    })
    ).toEqual(expectedState);
  });

  it('should add topics to the discussion when a topic is posted to the API', () => {
    const newTopicId = 'abc-123';
    const discussionId = 'def-456';
    const existingState = {
      byId: {
        [discussionId]: {
          id: discussionId,
          title: 'An Existing Discussion'
        }
      }
    }
    var expectedState = {
      byId: {
        [discussionId]: {
          id: discussionId,
          title: 'An Existing Discussion',
          topics: [newTopicId]
        }
      }
    };

    const result = reduce(existingState, {
      type: types.POST_TOPIC_SUCCESS,
      payload: {
        id: newTopicId,
        discussionId: discussionId,
        title: 'A New Topic'
      }
    });
    expect(result).toEqual(expectedState);
  });

  it('should set the discussions from a fetched board into the state', () => {
    const testDiscussionTitle = 'An Existing Board';
    const testDiscussionId = 'def-456';
    const testDiscussionIndex = 99;
    const apiResponse = {
      result: 'abc-123',
      entities: {
        discussions: {
          [testDiscussionId]: {
            id: testDiscussionId,
            title: testDiscussionTitle,
            index: testDiscussionIndex
          }
        }
      }
    };
    const expectedState = {
      byId: {
        [testDiscussionId]: {
          id: testDiscussionId,
          title: testDiscussionTitle,
          index: testDiscussionIndex
        }
      },
      maxIndex: testDiscussionIndex
    };
    expect(reduce({}, { type: types.GET_BOARD_SUCCESS, payload: apiResponse })).toEqual(expectedState);
  });

  it('should set an updated discussion into the state', () => {
    const testDiscussionTitle = 'An Updated Discussion';
    const testDiscussionId = 'def-456';
    const testTopicId = '123456';
    const testIndex = 99;
    const existingState = {
      byId: {
        [testDiscussionId]: {
          id: testDiscussionId,
          title: 'An Existing Discussion',
          index: 98
        }
      }
    }
    const apiResponse = {
      id: testDiscussionId,
      title: testDiscussionTitle,
      index: testIndex,
      topics: [testTopicId]
    };
    const expectedState = {
      byId: {
        [testDiscussionId]: {
          id: testDiscussionId,
          title: testDiscussionTitle,
          index: testIndex,
          topics: [testTopicId]
        }
      },
      maxIndex: testIndex
    };
    expect(reduce(existingState, { type: types.PUT_DISCUSSION_SUCCESS, payload: apiResponse })).toEqual(expectedState);
  });
});

describe('discussions selectors', () => {
  const firstDiscussionId = 'abc-def';
  const firstDiscussionTopics = ['123', '789'];
  const testState = {
    discussions: {
      byId: {
        '123-456': {
          id: '123-456',
          title: 'To Discuss',
          index: 5,
          topics: ['456', '012']
        },
        [firstDiscussionId]: {
          id: firstDiscussionId,
          title: 'Discussing',
          index: 4,
          topics: firstDiscussionTopics
        },
        '789-ghi': {
          id: '789-ghi',
          title: 'Done',
          index: 6,
          topics: ['345', '678']
        }
      },
      maxIndex: 6
    }
  };
  const testProps = { discussionId: firstDiscussionId };

  it('should get discussions by ID', () => {
    expect(getDiscussionsById(testState)).toEqual(testState.discussions.byId);
  });

  it('should get discussion ID array', () => {
    expect(getDiscussionsIdArray(testState)).toEqual(['abc-def', '123-456', '789-ghi']);
  });

  it('should return an empty state', () => {
    expect(getDiscussionsById({})).toEqual({});
  });

  it('should return an empty array', () => {
    expect(getDiscussionsIdArray({})).toEqual([]);
  });

  it('should return the new max index for the discussions', () => {
    expect(getNextDiscussionIndex({ discussions: { maxIndex: 98 } })).toEqual(99);
  });

  it('should return a list of topics associated with a particular discussion', () => {
    expect(getDiscussionTopics(testState, testProps)).toBe(firstDiscussionTopics);
  });
});