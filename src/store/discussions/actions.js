import * as types from '../actionTypes';
import cuid from 'cuid';
import { getJSON, CALL_API } from 'redux-api-middleware';
import _ from 'lodash';
const baseUrl = process.env.REACT_APP_BASE_API_URL || 'http://localhost:8080/api/v1';

export function createDiscussion(boardId, title, index) {
  return {
    type: types.CREATE_DISCUSSION,
    payload: {
      boardId: boardId,
      discussionId: cuid(),
      title: title,
      index: index
    }
  };
}

export function moveTopic(sourceDiscussionId, destDiscussionId, topicId) {
  return {
    type: types.MOVE_TOPIC,
    payload: {
      topicId: topicId,
      sourceDiscussionId: sourceDiscussionId,
      destDiscussionId: destDiscussionId
    }
  }
}

export function createUserDiscussion(boardId, title, index) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/boards/${boardId}/discussions`,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ title, index }),
      types: [
        types.POST_DISCUSSION_REQUEST,
        {
          type: types.POST_DISCUSSION_SUCCESS,
          payload: (action, state, res) => getJSON(res).then((json) => { return { ...json, boardId } })
        },
        types.POST_DISCUSSION_FAILURE
      ]
    }
  }
}

export function getDiscussions(boardId) {
  return {
    [CALL_API]: {
      endpoint: `${baseUrl}/boards/${boardId}/discussions`,
      method: 'GET',
      types: [
        types.GET_DISCUSSIONS_REQUEST,
        types.GET_DISCUSSIONS_SUCCESS,
        types.GET_DISCUSSIONS_FAILURE
      ]
    }
  }
}

export function moveUserTopic(boardId, sourceDiscussionId, destDiscussionId, topicId) {
  return async (dispatch, getState) => {
    const discussions = getState().discussions;
    if (!discussions || !discussions.byId) {
      return;
    }
    let sourceDiscussion = discussions.byId[sourceDiscussionId];
    let destDiscussion = discussions.byId[destDiscussionId];
    if (!sourceDiscussion || !destDiscussion) {
      return;
    }
    if (!sourceDiscussion.topics || sourceDiscussion.topics.indexOf(topicId) === -1) {
      return;
    }
    sourceDiscussion.topics = _.pull(sourceDiscussion.topics, topicId);
    if (!destDiscussion.topics) {
      destDiscussion.topics = [];
    }
    destDiscussion.topics = _.concat(destDiscussion.topics, topicId);

    dispatch({
      [CALL_API]: {
        endpoint: `${baseUrl}/boards/${boardId}/discussions/${sourceDiscussionId}`,
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(sourceDiscussion),
        types: [
          types.PUT_DISCUSSION_REQUEST,
          types.PUT_DISCUSSION_SUCCESS,
          types.PUT_DISCUSSION_FAILURE
        ]
      }
    });

    dispatch({
      [CALL_API]: {
        endpoint: `${baseUrl}/boards/${boardId}/discussions/${destDiscussionId}`,
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(destDiscussion),
        types: [
          types.PUT_DISCUSSION_REQUEST,
          types.PUT_DISCUSSION_SUCCESS,
          types.PUT_DISCUSSION_FAILURE
        ]
      }
    });

  }
}