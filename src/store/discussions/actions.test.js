import * as discussions from './actions';
import * as types from '../actionTypes';
import thunk from 'redux-thunk';
import { apiMiddleware } from 'redux-api-middleware';
import configureMockStore from 'redux-mock-store';
import * as testState from '../testState.test';

const middleware = [thunk, apiMiddleware];
const createMockStore = configureMockStore(middleware);
let store;

const mockResponse = (status, statusText, response) => {
  return new window.Response(response, {
    status: status,
    statusText: statusText,
    headers: {
      'Content-type': 'application/json'
    }
  });
};

function successResponse(response) {
  return () => Promise.resolve(mockResponse(200, null, JSON.stringify(response)));
};

function failureResponse(status) {
  return () => Promise.resolve(mockResponse(status, 'A Test Error', JSON.stringify({})));
};

jest.mock('cuid', () => {
  return jest.fn(() => 'def-456');
});
describe('Discussion actions', () => {

  beforeEach(() => {
    store = createMockStore(testState.state);
  });

  it('should return an object with type CREATE_DISCUSSION when a discussion is created', () => {
    const boardId = '123';
    const title = 'A New Discussion Title';
    const index = 99;
    const expectedAction = {
      type: types.CREATE_DISCUSSION,
      payload: {
        boardId: boardId,
        discussionId: 'def-456',
        title: title,
        index: index
      }
    }
    expect(discussions.createDiscussion(boardId, title, index)).toEqual(expectedAction);
  });

  it('should return an object with type MOVE_TOPIC when a topic is moved', () => {
    const source = 'abc-123';
    const dest = 'def-456';
    const topic = '123456';
    const expectedAction = {
      type: types.MOVE_TOPIC,
      payload: {
        sourceDiscussionId: source,
        destDiscussionId: dest,
        topicId: topic
      }
    }
    expect(discussions.moveTopic(source, dest, topic)).toEqual(expectedAction);
  });

  it('should call the correct endpoint with the board ID and discussion title when a discussion is created', async () => {
    const testBoardId = '123';
    const testTitle = 'A New Discussion Title';
    const mockFetch = jest.fn().mockImplementation(successResponse({}));
    window.fetch = mockFetch;

    await store.dispatch(discussions.createUserDiscussion(testBoardId, testTitle));

    expect(mockFetch.mock.calls.length).toEqual(1);
    expect(mockFetch.mock.calls[0].length).toEqual(2);
    expect(mockFetch.mock.calls[0][0]).toEqual(`http://localhost:8080/api/v1/boards/${testBoardId}/discussions`);
    expect(mockFetch.mock.calls[0][1].method).toEqual('POST');
    expect(mockFetch.mock.calls[0][1].body).toEqual(JSON.stringify({ title: testTitle }));
  });

  it('should dispatch POST_DISCUSSION_SUCCESS when a discussion is created', async () => {
    const testBoardId = '123';
    const testTitle = 'A New Discussion Title';
    const testIndex = 99;
    const apiResponse = { id: 'abc123', title: testTitle, index: testIndex };
    const expectedActions = [
      { type: types.POST_DISCUSSION_REQUEST },
      { type: types.POST_DISCUSSION_SUCCESS, payload: { ...apiResponse, boardId: testBoardId }, meta: undefined }
    ]
    const expectedBody = { title: testTitle, index: testIndex };
    const mockFetch = jest.fn().mockImplementation(successResponse(apiResponse));
    window.fetch = mockFetch;

    await store.dispatch(discussions.createUserDiscussion(testBoardId, testTitle, testIndex));

    expect(store.getActions()).toEqual(expectedActions);
    expect(mockFetch.mock.calls[0][1].body).toBe(JSON.stringify(expectedBody));
  });

  it('should dispatch POST_DISCUSSION_FAILURE when a discussion cannot be created', async () => {
    const testBoardId = '123';
    const title = 'A New Discussion Title';
    const testIndex = 99;
    window.fetch = jest.fn().mockImplementation(failureResponse(400));

    await store.dispatch(discussions.createUserDiscussion(testBoardId, title, testIndex));

    expect(store.getActions().length).toEqual(2);
    expect(store.getActions()[1].type).toEqual(types.POST_DISCUSSION_FAILURE);
  });

  it('should call the correct endpoint with the board ID when discussions are fetched', async () => {
    const testBoardId = '123';
    const mockFetch = jest.fn().mockImplementation(successResponse({}));
    window.fetch = mockFetch;

    await store.dispatch(discussions.getDiscussions(testBoardId));

    expect(mockFetch.mock.calls.length).toEqual(1);
    expect(mockFetch.mock.calls[0].length).toEqual(2);
    expect(mockFetch.mock.calls[0][0]).toEqual(`http://localhost:8080/api/v1/boards/${testBoardId}/discussions`);
    expect(mockFetch.mock.calls[0][1].method).toEqual('GET');
  });

  it('should dispatch GET_DISCUSSIONS_SUCCESS when a discussions are fetched', async () => {
    const apiResponse = {
      boards: [
        { id: 'abc123', title: 'A Test Title' },
        { id: 'def456', title: 'Another Test Title' }
      ]
    }
    const expectedActions = [
      { type: types.GET_DISCUSSIONS_REQUEST },
      { type: types.GET_DISCUSSIONS_SUCCESS, payload: apiResponse, meta: undefined }
    ]
    window.fetch = jest.fn().mockImplementation(successResponse(apiResponse));

    await store.dispatch(discussions.getDiscussions());

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch GET_DISCUSSIONS_FAILURE when discussions cannot be fetched', async () => {
    window.fetch = jest.fn().mockImplementation(failureResponse(400));

    await store.dispatch(discussions.getDiscussions());

    expect(store.getActions().length).toEqual(2);
    expect(store.getActions()[1].type).toEqual(types.GET_DISCUSSIONS_FAILURE);
  });

  it('should update the discussions on the server when a topic is moved', async () => {
    const apiResponse = {}
    const expectedActions = [
      { type: types.PUT_DISCUSSION_REQUEST },
      { type: types.PUT_DISCUSSION_REQUEST }
    ]
    window.fetch = jest.fn().mockImplementation(successResponse(apiResponse));

    await store.dispatch(discussions.moveUserTopic(testState.firstBoardId, testState.firstDiscussionId, testState.secondDiscussionId, testState.firstTopicId));

    expect(store.getActions()).toEqual(expectedActions);
  });
});