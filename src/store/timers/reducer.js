import * as types from '../actionTypes';

const initialState = {
  initial: 0,
  elapsed: 0,
  time: 0,
  isRunning: false
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.START_TIMER:
      return {
        ...state,
        initial: action.initial,
        elapsed: action.initial,
        isRunning: true
      };
    case types.SET_ELAPSED:
      return {
        ...state,
        elapsed: action.elapsed
      };
    case types.SET_TIME:
      return {
        ...state,
        time: action.time
      };
    case types.TOGGLE_TIMER:
      return {
        ...state,
        isRunning: action.isRunning
      };
    case types.ADD_TIME:
      return {
        ...state,
        time: state.time + (action.seconds * 1000)
      }
    default:
      return state;
  }
}

export function getElapsed(state) {
  if (!state.timers || !state.timers.elapsed) {
    return 0;
  }
  return state.timers.elapsed;
}

export function getRemaining(state) {
  if (!state.timers) {
    return 0;
  }
  const currentTime = state.timers.time || 0;
  const elapsedInterval = (state.timers.elapsed - state.timers.initial) || 0;
  const timeRemaining = currentTime - elapsedInterval;
  return (timeRemaining > 0) ? timeRemaining : 0;
}

export function isRunning(state) {
  if (!state.timers || !state.timers.isRunning) {
    return false;
  }
  return state.timers.isRunning;
}