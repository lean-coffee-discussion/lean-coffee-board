import * as types from '../actionTypes'

export function setElapsed(elapsed) {
  return { type: types.SET_ELAPSED, elapsed }
}

export function startTimer(initial) {
  return { type: types.START_TIMER, initial }
}

export function setTime(time) {
  return { type: types.SET_TIME, time }
}

export function toggleTimer(isRunning) {
  return { type: types.TOGGLE_TIMER, isRunning }
}

export function addTime(seconds) {
  return { type: types.ADD_TIME, seconds }
}