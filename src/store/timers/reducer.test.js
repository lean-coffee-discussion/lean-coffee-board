import reduce, { getElapsed, getRemaining, startTimer, isRunning } from './reducer';
import * as types from '../actionTypes';

describe('boards reducer', () => {

  it('should return the initial state', () => {
    expect(reduce(undefined, {})).toEqual({ initial: 0, elapsed: 0, time: 0, isRunning: false })
  });

  it('should handle SET_ELAPSED with an initial time', () => {
    const testTime = new Date();
    const expectedState = {
      elapsed: testTime
    };
    expect(reduce([], {
      type: types.SET_ELAPSED,
      elapsed: testTime
    })
    ).toEqual(expectedState);
  });

  it('should handle SET_ELAPSED with an existing state', () => {
    const existingState = {
      initial: 0,
      elapsed: 0,
      time: 300
    };
    const testTime = new Date();
    const expectedState = {
      initial: 0,
      elapsed: testTime,
      time: 300
    };
    expect(reduce(
      existingState,
      { type: types.SET_ELAPSED, elapsed: testTime }
    )
    ).toEqual(expectedState);
  });

  it('should set the initial, elapsed time and isRunning when the timer is started', () => {
    const testTime = new Date();
    const expectedState = {
      initial: testTime,
      elapsed: testTime,
      isRunning: true
    };
    expect(reduce([], {
      type: types.START_TIMER,
      initial: testTime
    })
    ).toEqual(expectedState);
  });

  it('should set the initial, elapsed time and isRunning when the timer is started and there is an existing state', () => {
    const existingState = {
      initial: 0,
      elapsed: 0,
      isRunning: false,
      time: 300
    };
    const testTime = new Date();
    const expectedState = {
      initial: testTime,
      elapsed: testTime,
      isRunning: true,
      time: 300
    };
    expect(reduce(
      existingState,
      { type: types.START_TIMER, initial: testTime }
    )
    ).toEqual(expectedState);
  });

  it('should handle SET_TIME with an initial time', () => {
    const testTime = 300;
    const expectedState = {
      time: testTime
    };
    expect(reduce([], {
      type: types.SET_TIME,
      time: testTime
    })
    ).toEqual(expectedState);
  });

  it('should handle SET_TIME with an existing state', () => {
    const existingState = {
      time: 300
    };
    const testTime = 800;
    const expectedState = {
      time: testTime
    };
    expect(reduce(
      existingState,
      { type: types.SET_TIME, time: testTime }
    )
    ).toEqual(expectedState);
  });

  it('should handle STOP_TIMER with an existing state', () => {
    const existingState = {
      initial: 0,
      elapsed: 0,
      time: 500,
      isRunning: true
    };
    const expectedState = {
      initial: 0,
      elapsed: 0,
      time: 500,
      isRunning: false
    };
    expect(reduce(
      existingState,
      { type: types.TOGGLE_TIMER, isRunning: false }
    )
    ).toEqual(expectedState);
  });

  it('should add 3 minutes to the timer', () => {
    const existingState = {
      initial: 0,
      elapsed: 0,
      time: (5 * 60 * 1000),
      isRunning: false
    };
    const expectedState = {
      initial: 0,
      elapsed: 0,
      time: existingState.time + (3 * 60 * 1000),
      isRunning: false
    };
    expect(reduce(
      existingState,
      { type: types.ADD_TIME, seconds: (3 * 60) }
    )
    ).toEqual(expectedState);
  })
});

describe('Timer selectors', () => {
  it('should get the current elapsed time', () => {
    const currentTime = new Date();
    const currentState = {
      timers: {
        elapsed: currentTime
      }
    };
    expect(getElapsed(currentState)).toEqual(currentTime);
  });

  it('should get the remaining time', () => {
    const currentTime = Math.floor(new Date());
    const initialTime = currentTime - 300;
    const currentState = {
      timers: {
        initial: initialTime,
        elapsed: currentTime,
        time: 500,
        isRunning: true
      }
    };
    expect(getRemaining(currentState)).toEqual(200);
  });

  it('should get the current status of the timer', () => {
    const currentState = {
      timers: {
        isRunning: true
      }
    };
    expect(isRunning(currentState)).toBeTruthy();
  });

  it('should get the remaining time', () => {
    const totalTime = 500;
    const initialTime = 0;
    const currentTime = initialTime + totalTime + 1;
    const currentState = {
      timers: {
        initial: initialTime,
        elapsed: currentTime,
        time: totalTime,
        isRunning: true
      }
    };
    expect(getRemaining(currentState)).toEqual(0);
  });

  it('should handle an empty state', () => {
    expect(getElapsed({})).toBe(0);
    expect(getRemaining({})).toBe(0);
    expect(isRunning({})).toBe(false);
  });
});