import * as timers from './actions';
import * as types from '../actionTypes';

describe('Timer actions', () => {
  it('should return an object with type SET_ELAPSED', () => {
    const elapsedTime = new Date();
    const expectedAction = {
      type: types.SET_ELAPSED,
      elapsed: elapsedTime
    }
    expect(timers.setElapsed(elapsedTime)).toEqual(expectedAction);
  });

  it('should return an object with type START_TIMER', () => {
    const initialTime = new Date();
    const expectedAction = {
      type: types.START_TIMER,
      initial: initialTime
    }
    expect(timers.startTimer(initialTime)).toEqual(expectedAction);
  });

  it('should return an object with type SET_TIME', () => {
    const countdownTime = 300;
    const expectedAction = {
      type: types.SET_TIME,
      time: countdownTime
    }
    expect(timers.setTime(countdownTime)).toEqual(expectedAction);
  });

  it('should return an object with type TOGGLE_TIMER', () => {
    const expectedAction = {
      type: types.TOGGLE_TIMER,
      isRunning: true
    }
    expect(timers.toggleTimer(true)).toEqual(expectedAction);
  });

  it('should return an object with type ADD_TIME', () => {
    const secondsToAdd = 30;
    const expectedAction = {
      type: types.ADD_TIME,
      seconds: secondsToAdd
    }
    expect(timers.addTime(secondsToAdd)).toEqual(expectedAction);
  })
});