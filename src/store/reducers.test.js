import * as reducers from './reducers';

describe('reducers test', () => {
  it('should export boards reducers', () => {
    expect(reducers.boards).toBeDefined();
  });
  it('should export topics reducers', () => {
    expect(reducers.topics).toBeDefined();
  });
  it('should export timers reducers', () => {
    expect(reducers.timers).toBeDefined();
  });
  it('should export discussions reducers', () => {
    expect(reducers.discussions).toBeDefined();
  });
  it('should export user reducers', () => {
    expect(reducers.user).toBeDefined();
  });
});